# -*- coding: utf-8 -*-
import operator
import time
from collections import deque
from enum import Enum

from sra.python.Computer import Computer

class Coord:
    def __init__(self, x, y):
        self.x = int(x)
        self.y = int(y)
    def __eq__(self,other):
        return self.x == other.x and self.y == other.y
    def __ne__(self, other):
        return not (self == other)
    def clone(self):
        return Coord(self.x, self.y)
    def __add__(self, other):
        return Coord(self.x + other.x, self.y + other.y)
    def __hash__(self):
        return hash( (self.x, self.y) )

class Grid:
    def __init__(self):
        self.grid = {Coord(0,0):1}
        self.robot = Coord(0,0)
        self.tilechars = {0:'#', 1: '.', 2:'O'}
        self.dirs = {1:Coord(0,-1), 2:Coord(0,1), 3:Coord(-1,0), 4:Coord(1,0)}
        self.translate_dir = {'N':1, 'S': 2, 'W':3, 'E':4}
        self.opposite_dir = {0:0, 1:2, 2:1, 3:4, 4:3}

    def print_grid(self):
        minx = min([c.x for c in self.grid.keys()])
        maxx = max([c.x for c in self.grid.keys()])
        miny = min([c.y for c in self.grid.keys()])
        maxy = max([c.y for c in self.grid.keys()])
        for y in range(miny, maxy+1):
            line = ""
            for x in range(minx, maxx+1):
                c = Coord(x,y)
                if c==self.robot:
                    line += "R"
                elif not c in self.grid:
                    line += " "
                else:
                    line += self.tilechars[self.grid[c]]
            print (line)

    def add(self, c, value):
        self.grid[c] = value

    def update(self, output, dir_nr):
        self.add(self.robot+self.dirs[dir_nr], output)

class OxygenFound(Exception):
    pass

def get_dir_from_user(grid):
    while True:
        print("DIR: ", end="")
        dir_text = input()
        if dir_text in grid.translate_dir:
            return grid.translate_dir[dir_text]

def test_directions(computer, grid):
    for d in range(1, 5):
        c = computer.clone()
        c.input.appendleft(d)
        result, output = c.run_program(1)
        if result != Computer.State.Output:
            raise RuntimeError("Computer error!")
        grid.update(output[0], d)

def can_robot_move(grid, dir):
    newpos = grid.robot + grid.dirs[dir]
    return grid.grid[newpos] != 0


def move_robot(computer, grid, stack, backdir=0):
    test_directions(computer, grid)
    robot = grid.robot.clone()
    for d in range(1, 5):
        if grid.opposite_dir[backdir] == d:
            continue
        grid.robot = robot.clone()
        c = computer.clone()
        if can_robot_move(grid, d):
            c.input.appendleft(d)
            result, output = c.run_program(1)
            if result != Computer.State.Output:
                raise RuntimeError("Computer error!")
            if output[0] == 0:
                raise RuntimeError("Wall!")
            grid.robot = grid.robot + grid.dirs[d]
            move_robot(c, grid, stack+1, d)

def propagate_oxygen(grid):
    new_grid = grid.grid.copy()
    for c in grid.grid.keys():
        if grid.grid[c] == 2:
            for d in range(1,5):
                newpos = c + grid.dirs[d]
                if grid.grid[newpos] == 1:
                    new_grid[newpos] = 2
    grid.grid = new_grid.copy()

def run():
    text = open("input.txt").read()
    grid = Grid()
    computer = Computer(text)
    try:
        move_robot(computer, grid, 1)
    except OxygenFound:
        pass

    minutes = 0
    while list(grid.grid.values()).count(1)>0:
        propagate_oxygen(grid)
        minutes+=1

    grid.print_grid()
    print (minutes)




if __name__ == "__main__":
    start_time = time.time()
    run()
    print(time.time() - start_time)
