#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
from numpy import dot, arccos, degrees
from numpy.linalg import norm

field = []
tr = {'.': 0, '#': 1}

filename = 'input.txt'
#filename = 'testinput.txt'

for line in open(filename).readlines():
    row = []
    for c in line.strip():
        row.append(tr[c])
    field.append(row)

field = np.array(field, dtype=int)

target = np.where(field == 1)
positions = list(zip(target[0], target[1]))


def is_target_blocked(station, target, obstacle):
    v1 = [target[0]-station[0], target[1]-station[1]]
    v2 = [obstacle[0]-station[0], obstacle[1]-station[1]]
    nv1 = norm(v1)
    nv2 = norm(v2)
    cosphi = dot(v1, v2) / (nv1 * nv2)

    if np.isclose(cosphi, 1.0) and nv1 > nv2:
        return True
    else:
        return False


max_pos = None
max_stations = 0
for station in positions:
    visible = []
    for target in positions:
        if station == target:
            continue
        is_visible = True
        for obstacle in positions:
            if station == obstacle or target == obstacle:
                continue
            if is_target_blocked(station, target, obstacle):
                is_visible = False
                break
        if is_visible:
            visible.append(target)
    num = len(visible)
    print(station, num)
    if num > max_stations:
        max_pos = station
        max_stations = num

print('best: {} ({})'.format(max_pos, max_stations))
