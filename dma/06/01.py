#!/usr/bin/env python
# -*- coding: utf-8 -*-

orbits = {}

for line in open('input.txt'):
    center, orbiter = line.strip().split(')')
    orbits[orbiter] = center

num_orbits = 0

for orbiter, center in orbits.items():
    while True:
        num_orbits += 1
        orbiter = center
        if center == 'COM':
            break
        center = orbits[center]

print('num orbits: {}'.format(num_orbits))
