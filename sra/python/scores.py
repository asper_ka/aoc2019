# -*- coding: utf-8 -*-
import json
import operator
from datetime import datetime
from dateutil import tz

# convert time as described here:
# https://stackoverflow.com/questions/4770297/convert-utc-datetime-string-to-local-datetime
from_zone = tz.tzutc()
to_zone = tz.tzlocal()

def create_localtime_string(timestamp):
    utc_time = datetime.utcfromtimestamp(timestamp);
    utc_time = utc_time.replace(tzinfo=from_zone)
    local_time = utc_time.astimezone(to_zone)
    return local_time.strftime('%Y-%m-%d %H:%M:%S')

# get the JSON from aoc homepage:
# https://adventofcode.com/2019/leaderboard/private/view/454363.json
# But please note the following statement from aoc homepage
# "Please don't make frequent automated requests to this
# service - avoid sending requests more often than once every 15 minutes (900 seconds)."

scores = json.load(open("454363.json"))
members = scores['members']
events = {}
member_score = {}
for m in members:
    data = members[m]
    name = data['name']
    member_score[name] = 0
    completions = data['completion_day_level']
    for day in completions:
        parts = completions[day]
        for p in parts:
            time = int(parts[p]['get_star_ts'])
            events[time] = (name,int(day),int(p))

points = {}
for d in range(25):
    points[d+1] = [len(members), len(members)]
for e in sorted(events.keys()):
    name, day, part = events[e]
    score = points[day][part-1]
    member_score[name] += score
    points[day][part - 1] -= 1
    print("{0} solves day {1}, part {2} at {3} and gets {4} point(s)"
          .format(name, day, part, create_localtime_string(e), score))
sorted_x = sorted(member_score.items(), key=operator.itemgetter(1), reverse=True)
for m in sorted_x:
    print ("{0}: {1} points".format(m[0], m[1]))