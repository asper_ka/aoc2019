# -*- coding: utf-8 -*-

import time
from collections import deque
from itertools import permutations

#test_input = """3,15,3,16,1002,16,10,16,1,16,15,15,4,15,99,0,0"""
#test_input = """3,23,3,24,1002,24,10,24,1002,23,-1,23,101,5,23,23,1,24,23,23,4,23,99,0,0"""
#test_input = """3,31,3,32,1002,32,10,32,1001,31,-2,31,1007,31,0,33,1002,33,7,33,1,33,31,31,1,32,31,31,4,31,99,0,0,0"""

class computer:
    def __init__(self, text):
        self.program = [ int(x) for x in text.split(",") ]
        self.ip = 0
        pass

    def parse_instruction(self):
        text = str(self.program[self.ip])
        opcode = int(text[-2:])
        modes = []
        for i in range(len(text) - 3, -1, -1):
            modes.append(int(text[i]))
        for i in range(len(modes), 3):
            modes.append(0)
        return opcode, modes

    def get_value(self, param, mode):
        if (mode == 0):
            return self.program[param]
        else:
            return param

    def run_program (self, input):
        output=[]
        while True:
            opcode, modes = self.parse_instruction()
            if opcode==99:
                return True, output
            if opcode==1:
                params = self.program[self.ip + 1: self.ip + 4]
                self.program[params[2]]=self.get_value(params[0], modes[0])+self.get_value(params[1], modes[1])
                self.ip+=4
            elif opcode == 2:
                params = self.program[self.ip + 1: self.ip + 4]
                self.program[params[2]] = self.get_value(params[0], modes[0]) * self.get_value(params[1], modes[1])
                self.ip += 4
            elif opcode == 3:
                param = self.program[self.ip + 1]
                i = input.pop()
                self.program[param] = i
                print ("input ", i)
                self.ip += 2
            elif opcode == 4:
                param = self.program[self.ip + 1]
                output.append(self.get_value(param, modes[0]))
                self.ip += 2
            elif opcode == 5:
                params = self.program[self.ip + 1:self.ip + 3]
                if self.get_value(params[0], modes[0]) != 0:
                    self.ip = self.get_value(params[1], modes[1])
                else:
                    self.ip += 3
            elif opcode == 6:
                params = self.program[self.ip + 1:self.ip + 3]
                if self.get_value(params[0], modes[0]) == 0:
                    self.ip = self.get_value(params[1], modes[1])
                else:
                    self.ip += 3
            elif opcode == 7:
                params = self.program[self.ip + 1:self.ip + 4]
                if self.get_value(params[0], modes[0]) < self.get_value(params[1], modes[1]):
                    self.program[params[2]] = 1
                else:
                    self.program[params[2]] = 0
                self.ip += 4
            elif opcode == 8:
                params = self.program[self.ip + 1:self.ip + 4]
                if self.get_value(params[0], modes[0]) == self.get_value(params[1], modes[1]):
                    self.program[params[2]] = 1
                else:
                    self.program[params[2]] = 0
                self.ip += 4

            else:
                print("ERROR")
                return False, output



def run():
    text = open("input.txt").read()
    #text = test_input
    phases = permutations(range(0,5))
    max_output = 0
    for phase in phases:
        print (phase)
        last_output = run_phases(phase, text)
        if last_output > max_output:
            max_output = last_output
    print (max_output)


    #for phase_input in phases:
    #    print(phase)
    print(phases)


def run_phases(phase, text):
    last_output = 0
    for p in phase:
        c = computer(text)
        input = deque()
        input.appendleft(p)
        input.appendleft(last_output)
        result, output = c.run_program(input)
        if result == False:
            print("ERROR")
        print(output)
        last_output = output[0]
    return last_output


if __name__ == "__main__":
    start_time = time.time()
    run()
    print(time.time() - start_time)
