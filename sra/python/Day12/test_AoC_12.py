from unittest import TestCase

from sra.python.Day12.AoC_12 import parse_line, update_vel


class Test(TestCase):
    def test_parse_line(self):
        self.assertEqual(parse_line("<x=1, y=2, z=3>"), (1, 2, 3))
        self.assertEqual(parse_line("<x=-1, y=0, z=2>"), (-1, 0, 2))
        self.assertEqual(parse_line("<x=2, y=-10, z=-7>"), (2, -10, -7))
        self.assertEqual(parse_line("<x=4, y=-8, z=8>"), (4, -8, 8))
        self.assertEqual(parse_line("<x=3, y=5, z=-1>"), (3, 5, -1))

    def test_update_vel(self):
        planets= [(0,0,0), (10,-10,0)]
        vels = [(0,0,0), (0,0,0)]
        update_vel(0,1,planets, vels)
        self.assertEqual([(1,-1,0), (-1,1,0)], vels)
        update_vel(0,1,planets, vels)
        self.assertEqual([(2,-2,0), (-2,2,0)], vels)
