from unittest import TestCase

from sra.python.Day10.AoC_10 import gcd, get_dir_vector, create_grid, in_sight, Coord

test_grid = """.#..#
.....
#####
....#
...##"""


class Test(TestCase):
    def test_gcd(self):
        self.assertEqual(gcd(10, 4), 2)
        self.assertEqual(gcd(96, 80), 16)
        self.assertEqual(gcd(17, 4), 1)

    def test_get_dir_vector(self):
        self.assertEqual(get_dir_vector(10, 4), (5, 2))
        self.assertEqual(get_dir_vector(12, 4), (3, 1))
        self.assertEqual(get_dir_vector(17, 4), (17, 4))

    def test_create_grid(self):
        grid = create_grid(test_grid)
        self.assertEqual(len(grid), 5)
        self.assertEqual(grid[0], ".#..#")

    def test_in_sight(self):
        grid = create_grid(test_grid)
        self.assertTrue(in_sight(Coord(1,0),Coord(4,0),grid))
        self.assertTrue(in_sight(Coord(0,0),Coord(1,2),grid))
        self.assertFalse(in_sight(Coord(3,4),Coord(1,0),grid))
        self.assertTrue(in_sight(Coord(3,4),Coord(4,0),grid))
