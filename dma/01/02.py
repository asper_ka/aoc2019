#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np

masses = np.loadtxt('input1')

def calc_fuel(mass):
    fuel = int(np.floor(mass / 3.0) -2)
    fuel = fuel if fuel > 0 else 0
    print(fuel)
    return fuel
    

total_fuel = 0
for mass in masses:
    cur_mass = mass
    while cur_mass > 0:
        fuel = calc_fuel(cur_mass)
        total_fuel += fuel
        cur_mass = fuel
    
print(total_fuel)

