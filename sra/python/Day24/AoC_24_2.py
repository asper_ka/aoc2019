# -*- coding: utf-8 -*-

import time

test_input = """....#
#..#.
#.?##
..#..
#...."""

class Grid():
    def __init__(self, sizex = 0, sizey = 0):
        self.sizex = sizex
        self.sizey = sizey
        self.g = {}


def create_grid(text):
    lines = text.split("\n")
    sizey = len(lines)
    sizex = len(lines[0])
    grid = Grid(sizex, sizey)
    for y in range(sizey):
        for x in range(sizex):
            if lines[y][x] == "#":
                grid.g[(x, y, 0)] = 1
    return grid


def print_grid(grid, level):
    for y in range(grid.sizey):
        text = ""
        for x in range(grid.sizex):
            if (x, y, level) in grid.g:
                text += "#"
            else:
                text += "."
        print(text)

def get_neighbors(x,y,level, sizex, sizey):
    nbs = []
    dirs = [(-1,0), (1,0), (0,-1), (0,1)]
    for (dx, dy) in dirs:
        nx = x+dx
        ny = y + dy
        if nx == -1:
            nbs.append((1,2,level-1))
        elif nx == sizex:
            nbs.append((3, 2, level - 1))
        elif ny == -1:
            nbs.append((2, 1, level - 1))
        elif ny == sizey:
            nbs.append((2, 3, level - 1))
        elif nx==2 and ny ==2:
            if x==1:
                for i in range(5):
                    nbs.append((0,i,level+1))
            elif x == 3:
                for i in range(5):
                    nbs.append((4, i, level + 1))
            elif y == 1:
                for i in range(5):
                    nbs.append((i, 0, level + 1))
            elif y == 3:
                for i in range(5):
                    nbs.append((i, 4, level + 1))
        else:
            nbs.append((nx, ny, level))
    return nbs

def create_next_gen(grid):
    new_grid = Grid(grid.sizex, grid.sizey)
    levels = [c[2] for c in grid.g.keys()]
    minlevel = min(levels)-1
    maxlevel = max(levels)+1
    for level in range(minlevel, maxlevel+1):
        for y in range(grid.sizey):
            for x in range(grid.sizex):
                if (x!=2) or (y!=2):
                    nbs = get_neighbors(x,y,level,grid.sizex, grid.sizey)
                    cnt_nbs = 0
                    for n in nbs:
                        if n in grid.g:
                            cnt_nbs += 1
                    if (x, y, level) in grid.g and cnt_nbs == 1:
                        new_grid.g[(x, y, level)] = 1
                    elif not (x, y, level) in grid.g and (cnt_nbs == 1 or cnt_nbs == 2):
                        new_grid.g[(x, y, level)] = 1
    return new_grid

def run():
    text = open("input.txt").read().strip()
    #text = test_input
    grid = create_grid(text)
    for m in range (200):
        grid = create_next_gen(grid)

    #for l in range (-5,6):
    #    print (l)
    #    print_grid(grid, l)

    print (len(grid.g))
    #print (grid.g)


if __name__ == "__main__":
    start_time = time.time()
    run()
    print(time.time() - start_time)
