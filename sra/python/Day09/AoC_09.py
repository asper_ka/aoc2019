# -*- coding: utf-8 -*-

import time
from collections import deque
from enum import Enum

class Computer:
    def __init__(self, text):
        self.program = [ int(x) for x in text.split(",") ]
        for i in range(1000):
            self.program.append(0)
        self.ip = 0
        self.input = deque()
        self.relative_base = 0
        pass
    class State(Enum):
        Output = 1
        Error = 2
        Halt = 3

    def parse_instruction(self):
        text = str(self.program[self.ip])
        opcode = int(text[-2:])
        modes = []
        for i in range(len(text) - 3, -1, -1):
            modes.append(int(text[i]))
        for i in range(len(modes), 3):
            modes.append(0)
        return opcode, modes

    def get_value(self, param, mode):
        if (mode == 0):
            return self.program[param]
        if (mode == 2):
            return self.program[self.relative_base + param]
        else:
            return param

    def set_value(self, param, mode, value):
        if (mode == 0):
            self.program[param] = value
        if (mode == 2):
            self.program[self.relative_base + param] = value

    def run_program (self):
        output=[]
        while True:
            opcode, modes = self.parse_instruction()
            if opcode==99:
                return self.State.Halt, output
            if opcode==1:
                params = self.program[self.ip + 1: self.ip + 4]
                self.set_value(params[2], modes[2],
                               self.get_value(params[0], modes[0])+self.get_value(params[1], modes[1]))
                self.ip+=4
            elif opcode == 2:
                params = self.program[self.ip + 1: self.ip + 4]
                self.set_value(params[2], modes[2],
                               self.get_value(params[0], modes[0]) * self.get_value(params[1], modes[1]))
                self.ip += 4
            elif opcode == 3:
                param = self.program[self.ip + 1]
                i = self.input.pop()
                self.set_value(param, modes[0],i)
                #print ("input ", i)
                self.ip += 2
            elif opcode == 4:
                param = self.program[self.ip + 1]
                o = self.get_value(param, modes[0])
                #print ("output ", o)
                output.append(o)
                self.ip += 2
                #return self.State.Output, output
            elif opcode == 5:
                params = self.program[self.ip + 1:self.ip + 3]
                if self.get_value(params[0], modes[0]) != 0:
                    self.ip = self.get_value(params[1], modes[1])
                else:
                    self.ip += 3
            elif opcode == 6:
                params = self.program[self.ip + 1:self.ip + 3]
                if self.get_value(params[0], modes[0]) == 0:
                    self.ip = self.get_value(params[1], modes[1])
                else:
                    self.ip += 3
            elif opcode == 7:
                params = self.program[self.ip + 1:self.ip + 4]
                if self.get_value(params[0], modes[0]) < self.get_value(params[1], modes[1]):
                    self.set_value(params[2], modes[2], 1)
                else:
                    self.set_value(params[2], modes[2], 0)
                self.ip += 4
            elif opcode == 8:
                params = self.program[self.ip + 1:self.ip + 4]
                if self.get_value(params[0], modes[0]) == self.get_value(params[1], modes[1]):
                    self.set_value(params[2], modes[2], 1)
                else:
                    self.set_value(params[2], modes[2], 0)
                self.ip += 4
            elif opcode == 9:
                param = self.program[self.ip + 1]
                self.relative_base += self.get_value(param, modes[0])
                self.ip += 2

            else:
                print("ERROR")
                return self.State.Error, output



def run():
    text = open("input.txt").read()
    computer = Computer(text)
    computer.input.appendleft(2)
    result, output = computer.run_program()
    print (output)


if __name__ == "__main__":
    start_time = time.time()
    run()
    print(time.time() - start_time)
