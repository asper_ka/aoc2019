# -*- coding: utf-8 -*-

import time
from sra.python.Computer import Computer



def run():
    text = open("input.txt").read().strip()
    computers = []
    for i in range(50):
        computer = Computer(text)
        computer.input.appendleft(i)
        computers.append(computer)

    messages = {}
    NAT = []
    NATsentY = set()
    while True:
        for i in range(50):
            if i in messages:
                #print ("Input for", i, ":", messages[i])
                for m in messages[i]:
                    computers[i].input.appendleft(m)
                del messages[i]
            else:
                computers[i].input.appendleft(-1)
            result, output = computers[i].run_program()
            #print (i, result, output, computers[i].ip, computers[i].relative_base)
            for i in range(0, len(output), 3):
                addr = output[0+i]
                #print(addr, ": ", output[1+i], output[2+i])
                if addr == 255:
                    print ("NAT message: ", output[1+i], output[2+i])
                    NAT = [output[1+i], output[2+i]]
                else:
                    if not addr in messages:
                        messages[addr] = []
                    messages[addr].append(output[1+i])
                    messages[addr].append(output[2+i])
            if result != Computer.State.WaitInput:
                print ("ERROR: ", i, result, output)
        #print (messages)
        if sum(len(m) for m in messages.values()) == 0:
            messages[0] = [NAT[0], NAT[1]]
            if NAT[1] in NATsentY:
                print ("sending the second time: ", NAT[1])
                return
            NATsentY.add(NAT[1])

if __name__ == "__main__":
    start_time = time.time()
    run()
    print(time.time() - start_time)
