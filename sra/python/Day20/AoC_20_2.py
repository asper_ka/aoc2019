# -*- coding: utf-8 -*-
import collections
import copy
import operator
import time


text_input = """         A           
         A           
  #######.#########  
  #######.........#  
  #######.#######.#  
  #######.#######.#  
  #######.#######.#  
  #####  B    ###.#  
BC...##  C    ###.#  
  ##.##       ###.#  
  ##...DE  F  ###.#  
  #####    G  ###.#  
  #########.#####.#  
DE..#######...###.#  
  #.#########.###.#  
FG..#########.....#  
  ###########.#####  
             Z       
             Z       """

text_input2 = """             Z L X W       C                 
             Z P Q B       K                 
  ###########.#.#.#.#######.###############  
  #...#.......#.#.......#.#.......#.#.#...#  
  ###.#.#.#.#.#.#.#.###.#.#.#######.#.#.###  
  #.#...#.#.#...#.#.#...#...#...#.#.......#  
  #.###.#######.###.###.#.###.###.#.#######  
  #...#.......#.#...#...#.............#...#  
  #.#########.#######.#.#######.#######.###  
  #...#.#    F       R I       Z    #.#.#.#  
  #.###.#    D       E C       H    #.#.#.#  
  #.#...#                           #...#.#  
  #.###.#                           #.###.#  
  #.#....OA                       WB..#.#..ZH
  #.###.#                           #.#.#.#  
CJ......#                           #.....#  
  #######                           #######  
  #.#....CK                         #......IC
  #.###.#                           #.###.#  
  #.....#                           #...#.#  
  ###.###                           #.#.#.#  
XF....#.#                         RF..#.#.#  
  #####.#                           #######  
  #......CJ                       NM..#...#  
  ###.#.#                           #.###.#  
RE....#.#                           #......RF
  ###.###        X   X       L      #.#.#.#  
  #.....#        F   Q       P      #.#.#.#  
  ###.###########.###.#######.#########.###  
  #.....#...#.....#.......#...#.....#.#...#  
  #####.#.###.#######.#######.###.###.#.#.#  
  #.......#.......#.#.#.#.#...#...#...#.#.#  
  #####.###.#####.#.#.#.#.###.###.#.###.###  
  #.......#.....#.#...#...............#...#  
  #############.#.#.###.###################  
               A O F   N                     
               A A D   M                     """


class Grid():
    def __init__(self, text=""):
        self.iportals = {}
        self.oportals = {}
        self.grid = {}
        self.maxx = 0
        self.maxy = 0
        self.create_grid(text)
        self.dirs = {1: Coord(0, -1), 2: Coord(0, 1), 3: Coord(-1, 0), 4: Coord(1, 0)}
        self.opposite_dir = {0: 0, 1: 2, 2: 1, 3: 4, 4: 3}

    def create_grid(self, text):
        lines = text.split("\n")
        if len(lines) == 0:
            return
        self.maxy = len(lines) - 1
        if lines[self.maxy].strip() == "":
            self.maxy -= 1
        self.maxx = len(lines[0]) - 1
        for y in range(self.maxy + 1):
            for x in range(self.maxx + 1):
                c = Coord(x, y)
                char = lines[y][x]
                if char == "." or char == "#":
                    self.grid[c] = char+char
        for y in range(self.maxy):
            for x in range(self.maxx):
                c = Coord(x, y)
                char = lines[y][x]
                if not char.isalpha():
                    continue
                to_test = []
                if lines[y][x+1].isalpha():
                    name = char + lines[y][x+1]
                    to_test = [Coord(x+2, y), Coord(x-1,y)]
                if lines[y+1][x].isalpha():
                    name = char + lines[y+1][x]
                    to_test = [Coord(x, y+2), Coord(x, y-1)]
                for c in to_test:
                    if c in self.grid:
                        if (c.x == 2) or (c.x == self.maxx-2) or (c.y == 2) or (c.y == self.maxy-2):
                            self.oportals[name] = c
                        else:
                            self.iportals[name] = c
                        self.grid[c] = name

    def __str__(self):
        text = ""
        for y in range(self.maxy + 1):
            for x in range(self.maxx + 1):
                c = Coord(x, y)
                if c in self.grid:
                    text += self.grid[c]
                else:
                    text += "  "
            text += "\n"
        return text



class Coord:
    def __init__(self, x, y):
        self.x = int(x)
        self.y = int(y)

    def __eq__(self, other):
        return self.x == other.x and self.y == other.y

    def __ne__(self, other):
        return not (self == other)

    def clone(self):
        return Coord(self.x, self.y)

    def __add__(self, other):
        return Coord(self.x + other.x, self.y + other.y)

    def __hash__(self):
        return hash((self.x, self.y))

    def __str__(self):
        return "({0}, {1})".format(self.x, self.y)

def find_neighbors(grid, pos, visited, level):
    neighbors = []
    for d in grid.dirs.keys():
        nb = pos + grid.dirs[d]
        if nb in grid.grid:
            char = grid.grid[nb]
            wall = (char == '##')
            if (level == 0) and char in grid.oportals:
                if grid.oportals[char] == nb:
                    wall = (char != "AA" and char != "ZZ")
            if (not wall) and (not (nb, level) in visited):
                neighbors.append((nb, level))
    char = grid.grid[pos]
    if char != "..":
        if char in grid.iportals and grid.iportals[char] == pos:
            if char in grid.oportals:
                nb = grid.oportals[char]
                if not (nb, level+1) in visited:
                    neighbors.append((nb, level+1))
        if char in grid.oportals and grid.oportals[char] == pos:
            if char in grid.iportals:
                nb = grid.iportals[char]
                if not (nb, level-1) in visited:
                    neighbors.append((nb, level - 1))
    return neighbors

def find(grid, target):
    queue = collections.deque()
    queue.append((grid.oportals["AA"], 0, 0))
    visited = set()
    visited.add((grid.oportals["AA"], 0))
    while len(queue)>0:
        pos, steps, level = queue.popleft()
        if (steps%100)==0:
            print (pos, steps, level)
        nb = find_neighbors(grid, pos, visited, level)
        for (n, l) in nb:
            if l == 0 and grid.grid[n] == target:
                print (steps+1)
                return
            queue.append((n, steps+1, l))
            visited.add((n, l))


def run():
    text = open("input.txt").read()
    #text = text_input2
    grid = Grid(text)
    print (grid)
    find(grid, "ZZ")

if __name__ == "__main__":
    start_time = time.time()
    run()
    print(time.time() - start_time)
