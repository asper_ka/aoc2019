#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np

code = np.genfromtxt('input1', delimiter=',', dtype=int)
code[1] = 12
code[2] = 2

#code = np.genfromtxt('testinput', delimiter=',', dtype=int)
#code = [1,0,0,0,99]
#code = [2,3,0,3,99]

pos = 0
while True:
    print(code)
    opcode = code[pos]
    if opcode == 99:
        print('halt')
        break
    pos1 = code[pos + 1]
    pos2 = code[pos + 2]
    out = code[pos+3]
    if opcode == 1:
        code[out] = code[pos1] + code[pos2]
    elif opcode == 2:
        code[out] = code[pos1] * code[pos2]
    pos += 4

print(code)

print('result: {}'.format(code[0]))
