# -*- coding: utf-8 -*-
import time

input_text = "03036732577212944063491565474664"

class FactorGenerator():
    def __init__(self, position):
        self.pattern = [0,1,0,-1]
        self.position = position
        self.index = 1
        self.sub_index = 0
    def get_next(self):
        if self.sub_index >= self.position:
            self.sub_index = 0
            self.index = (self.index + 1) % 4
        self.sub_index +=1
        return self.pattern[self.index]

def run():
    text = open("input.txt").read().strip()
    #text = input_text
    print (len(text))
    dim = len(text)
    result = [0] * dim * 10000
    idx = 0
    for c in text:
        for idx2 in range(10000):
            result[idx2*dim+idx] = int(c)
        idx+=1
    dim *= 10000
    message = [0] * dim
    for p in range (100):
        print (p,"/100")
        tmp = message
        message = result
        result = tmp
        # for pos in range (1,int(dim/2)+1):
        #     #print (pos)
        #     sum = 0
        #     idx = 0
        #     for c in message[pos-1:]:
        #         i = int(idx/pos)%4
        #         if i == 0:
        #             sum+= c
        #         elif i == 2:
        #             sum-=c
        #         idx+=1
        #     #print (" = {} => {}".format(sum, abs(sum)%10))
        #     result[pos-1]=((abs(sum)%10))
        sum = 0
        for pos in range (dim, int(dim/2), -1):
            sum += message[pos-1]
            result[pos-1]=((abs(sum)%10))
            pass
        #print (result)
    start_idx = int(text[:7])
    print (start_idx, result[start_idx:start_idx+8])

if __name__ == "__main__":
    start_time = time.time()
    run()
    print(time.time() - start_time)
