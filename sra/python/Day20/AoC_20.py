# -*- coding: utf-8 -*-
import copy
import operator
import time


text_input = """         A           
         A           
  #######.#########  
  #######.........#  
  #######.#######.#  
  #######.#######.#  
  #######.#######.#  
  #####  B    ###.#  
BC...##  C    ###.#  
  ##.##       ###.#  
  ##...DE  F  ###.#  
  #####    G  ###.#  
  #########.#####.#  
DE..#######...###.#  
  #.#########.###.#  
FG..#########.....#  
  ###########.#####  
             Z       
             Z       """

text_input2 = """                   A               
                   A               
  #################.#############  
  #.#...#...................#.#.#  
  #.#.#.###.###.###.#########.#.#  
  #.#.#.......#...#.....#.#.#...#  
  #.#########.###.#####.#.#.###.#  
  #.............#.#.....#.......#  
  ###.###########.###.#####.#.#.#  
  #.....#        A   C    #.#.#.#  
  #######        S   P    #####.#  
  #.#...#                 #......VT
  #.#.#.#                 #.#####  
  #...#.#               YN....#.#  
  #.###.#                 #####.#  
DI....#.#                 #.....#  
  #####.#                 #.###.#  
ZZ......#               QG....#..AS
  ###.###                 #######  
JO..#.#.#                 #.....#  
  #.#.#.#                 ###.#.#  
  #...#..DI             BU....#..LF
  #####.#                 #.#####  
YN......#               VT..#....QG
  #.###.#                 #.###.#  
  #.#...#                 #.....#  
  ###.###    J L     J    #.#.###  
  #.....#    O F     P    #.#...#  
  #.###.#####.#.#####.#####.###.#  
  #...#.#.#...#.....#.....#.#...#  
  #.#####.###.###.#.#.#########.#  
  #...#.#.....#...#.#.#.#.....#.#  
  #.###.#####.###.###.#.#.#######  
  #.#.........#...#.............#  
  #########.###.###.#############  
           B   J   C               
           U   P   P               """


class Grid():
    def __init__(self, text=""):
        self.portals = {}
        self.grid = {}
        self.maxx = 0
        self.maxy = 0
        self.create_grid(text)
        self.dirs = {1: Coord(0, -1), 2: Coord(0, 1), 3: Coord(-1, 0), 4: Coord(1, 0)}
        self.opposite_dir = {0: 0, 1: 2, 2: 1, 3: 4, 4: 3}

    def create_grid(self, text):
        lines = text.split("\n")
        if len(lines) == 0:
            return
        self.maxy = len(lines) - 1
        if lines[self.maxy].strip() == "":
            self.maxy -= 1
        self.maxx = len(lines[0]) - 1
        for y in range(self.maxy + 1):
            for x in range(self.maxx + 1):
                c = Coord(x, y)
                char = lines[y][x]
                if char == "." or char == "#":
                    self.grid[c] = char+char
        for y in range(self.maxy):
            for x in range(self.maxx):
                c = Coord(x, y)
                char = lines[y][x]
                if not char.isalpha():
                    continue
                to_test = []
                if lines[y][x+1].isalpha():
                    name = char + lines[y][x+1]
                    to_test = [Coord(x+2, y), Coord(x-1,y)]
                if lines[y+1][x].isalpha():
                    name = char + lines[y+1][x]
                    to_test = [Coord(x, y+2), Coord(x, y-1)]
                for c in to_test:
                    if c in self.grid:
                        if not name in self.portals:
                            self.portals[name] = []
                        self.portals[name].append(c)
                        self.grid[c] = name

    def __str__(self):
        text = ""
        for y in range(self.maxy + 1):
            for x in range(self.maxx + 1):
                c = Coord(x, y)
                if c in self.portals:
                    text += self.portals[c]
                elif c in self.grid:
                    text += self.grid[c]
                else:
                    text += "  "
            text += "\n"
        return text



class Coord:
    def __init__(self, x, y):
        self.x = int(x)
        self.y = int(y)

    def __eq__(self, other):
        return self.x == other.x and self.y == other.y

    def __ne__(self, other):
        return not (self == other)

    def clone(self):
        return Coord(self.x, self.y)

    def __add__(self, other):
        return Coord(self.x + other.x, self.y + other.y)

    def __hash__(self):
        return hash((self.x, self.y))

    def __str__(self):
        return "({0}, {1})".format(self.x, self.y)

def find_neighbors(grid, pos, visited):
    neighbors = []
    for d in grid.dirs.keys():
        nb = pos + grid.dirs[d]
        if nb in grid.grid and grid.grid[nb] != '##':
            if not nb in visited:
                neighbors.append(nb)
    if grid.grid[pos] in grid.portals:
        links = grid.portals[grid.grid[pos]]
        if len(links) > 1:
            if links[0] == pos:
                nb = links[1]
            else:
                nb = links[0]
            if not nb in visited:
                neighbors.append(nb)
    return neighbors

def find(grid, target):
    queue = [(grid.portals["AA"][0], 0)]
    visited = []
    while len(queue)>0:
        pos, steps = queue.pop(0)
        nb = find_neighbors(grid, pos, visited)
        for n in nb:
            if grid.grid[n] == target:
                print (steps+1)
                return
            queue.append((n, steps+1))
            visited.append(n)


def run():
    text = open("input.txt").read()
    #text = text_input2
    grid = Grid(text)
    print (grid)
    find(grid, "ZZ")

if __name__ == "__main__":
    start_time = time.time()
    run()
    print(time.time() - start_time)
