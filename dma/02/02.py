#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np

code = np.genfromtxt('input1', delimiter=',', dtype=int)
original_code = code.copy()
required = 19690720

noun = 93
verb = 42

code = original_code.copy()

code[1] = noun
code[2] = verb

#print(code)

pos = 0
while True:
    opcode = code[pos]
    if opcode == 99:
        break
    pos1 = code[pos + 1]
    pos2 = code[pos + 2]
    out = code[pos+3]
    if opcode == 1:
        code[out] = code[pos1] + code[pos2]
    elif opcode == 2:
        code[out] = code[pos1] * code[pos2]
    pos += 4

print('noun {}, verb {}, output: {}'.format(noun, verb, code[0]))
print('result: {}'.format(100*noun + verb))
