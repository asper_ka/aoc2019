# -*- coding: utf-8 -*-

import time

test_grid = """.#..#
.....
#####
....#
...##"""

test_grid2 = """.#..##.###...#######
##.############..##.
.#.######.########.#
.###.#######.####.#.
#####.##.#.##.###.##
..#####..#.#########
####################
#.####....###.#.#.##
##.#################
#####.##.###..####..
..######..##.#######
####.##.####...##..#
.#####..#.######.###
##...#.##########...
#.##########.#######
.####.#.###.###.#.##
....##.##.###..#####
.#.#.###########.###
#.#.#.#####.####.###
###.##.####.##.#..##"""

class Coord:
    def __init__(self, x, y):
        self.x = int(x)
        self.y = int(y)
    def equals(self,c):
        return self.x == c.x and self.y == c.y
    def clone(self):
        return Coord(self.x, self.y)

def gcd(x, y):
    while (y):
        x, y = y, x % y
    return x

def get_dir_vector(dx,dy):
    d = abs(gcd(dx,dy))
    return (int(dx/d), int(dy/d))

def create_grid(text):
    return text.split()

def in_sight(c1, c2, grid):
    (dx,dy) = get_dir_vector((c2.x-c1.x), (c2.y-c1.y))
    pos = c2.clone()
    while not pos.equals(c1):
        pos.x -= dx
        pos.y -= dy
        if not pos.equals(c1) and grid[pos.y][pos.x]=='#':
            return False
    return True

def count_in_sight(c1,grid):
    count = 0
    for y in range(len(grid)):
        for x in range(len(grid[0])):
            c2 = Coord(x,y)
            if not c1.equals(c2) and grid[y][x]=="#" and in_sight(c1,c2,grid):
                count += 1
    return count

def run():
    text = open("input.txt").read().strip()
    #text = test_grid2
    grid = create_grid(text)
    result = []
    max_count = 0
    best_pos = Coord(-1,-1)
    for y in range(len(grid)):
        result.append("")
        for x in range(len(grid[0])):
            c = 0
            if grid[y][x]=="#":
                c = count_in_sight(Coord(x,y),grid)
            if c==0:
                result[y] += "."
            else:
                result[y] += str(c)
            if c > max_count:
                max_count = c
                best_pos = Coord(x,y)
#    for l in result:
#        print (l)
    print (max_count, (best_pos.x, best_pos.y))


if __name__ == "__main__":
    start_time = time.time()
    run()
    print(time.time() - start_time)
