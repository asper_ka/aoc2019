# -*- coding: utf-8 -*-

import time
import networkx as nx

test_input = """COM)B
B)C
C)D
D)E
E)F
B)G
G)H
D)I
E)J
J)K
K)L"""

def get_objects(text):
    list = text.split(")")
    return list[0], list[1]


def parse_input(text):
    g = nx.Graph()
    objects = {}
    for line in text.split():
        g.add_edge(*get_objects(line))
    return g

def run():
    text = open("input.txt").read()
    #text = test_input
    g = parse_input(text)
    path_len = 0
    for n in g.nodes:
        if n != "COM":
            path = nx.dijkstra_path(g, 'COM', n)
            path_len += (len(path)-1)
    print (path_len)


if __name__ == "__main__":
    start_time = time.time()
    run()
    print(time.time() - start_time)
