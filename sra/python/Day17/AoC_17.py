# -*- coding: utf-8 -*-

import time
from sra.python.Computer import Computer


def print_grid(grid):
    minx = min([c[0] for c in grid.keys()])
    miny = min([c[1] for c in grid.keys()])
    maxx = max([c[0] for c in grid.keys()])
    maxy = max([c[1] for c in grid.keys()])
    for y in range(miny, maxy + 1):
        line = ""
        for x in range(minx, maxx + 1):
            line += grid[(x,y)]
        print(line)

def build_grid(output):
    x = 0
    y = 0
    grid = {}
    for c in output:
        if c == 10:
            x = 0
            y += 1
        else:
            grid[(x, y)] = str(chr(c))
            x += 1
    return grid

def find_intersections(grid):
    minx = min([c[0] for c in grid.keys()])
    miny = min([c[1] for c in grid.keys()])
    maxx = max([c[0] for c in grid.keys()])
    maxy = max([c[1] for c in grid.keys()])
    intersections = []
    for c in grid.keys():
        if grid[c] == '.' or c[0] == minx or c[0] == maxx or c[1]==miny or c[1]==minx:
            continue
        x, y = c
        if grid[(x-1,y)] != '.' and grid[(x+1,y)] != '.' and grid[(x,y-1)] != '.' and grid[x,y+1] != '.':
            intersections.append((x,y))
    return intersections

def run():
    text = open("input.txt").read().strip()
    computer = Computer(text)
    result, output = computer.run_program()
    grid = build_grid(output)

    print_grid(grid)
    intersections = find_intersections(grid)
    print (sum([c[0]*c[1] for c in intersections]))



if __name__ == "__main__":
    start_time = time.time()
    run()
    print(time.time() - start_time)
