#!/usr/bin/env python
# -*- coding: utf-8 -*-

import collections

start = 152085
stop = 670283

num_matching = 0

for num in range(start, stop+1):
    repeated = collections.Counter()
    last_digit = None
    decreasing = False
    for digit in str(num):
        if last_digit is not None:
            if digit == last_digit:
                if digit not in repeated:
                    repeated[digit] = 1
                repeated[digit] += 1

            if int(digit) < int(last_digit):
                decreasing = True
        last_digit = digit

    if not decreasing and repeated:
        if 2 in repeated.values():
            print(num, repeated)
            num_matching += 1

print(num_matching)
