# -*- coding: utf-8 -*-

import time
from sra.python.Computer import Computer

class Coord:
    def __init__(self, x, y):
        self.x = int(x)
        self.y = int(y)
    def __eq__(self,other):
        return self.x == other.x and self.y == other.y
    def __ne__(self, other):
        return not (self == other)
    def clone(self):
        return Coord(self.x, self.y)
    def __add__(self, other):
        return Coord(self.x + other.x, self.y + other.y)
    def __hash__(self):
        return hash( (self.x, self.y) )
    def __str__(self):
        return "({0}, {1})".format(self.x, self.y)


def print_grid(grid, robot):
    minx = min([c.x for c in grid.keys()])
    miny = min([c.y for c in grid.keys()])
    maxx = max([c.x for c in grid.keys()])
    maxy = max([c.y for c in grid.keys()])
    for y in range(miny, maxy + 1):
        line = ""
        for x in range(minx, maxx + 1):
            if Coord(x,y) == robot.pos:
                line += robot.dirs[robot.dir]
            else:
                line += grid[Coord(x,y)]
        print(line)

class Robot():
    def __init__(self, pos=Coord(0,0), dir="^"):
        self.pos = pos
        self.dir_chars = {"^":0, "v":2, "<":3, ">":1}
        self.dir = self.dir_chars[dir]
        self.dir_offsets = {0:Coord(0,-1), 2:Coord(0,1), 3:Coord(-1,0), 1:Coord(1,0)}
        self.dirs = {0:"^", 1:">", 2:"v", 3:"<"}

    def turn_right(self):
        self.dir = (self.dir+1)%4
    def turn_left(self):
        self.dir = (4+self.dir-1)%4
    def move(self):
        self.pos = self.pos + self.dir_offsets[self.dir]

    def get_front(self,grid):
        c = self.pos + self.dir_offsets[self.dir]
        if not c in grid:
            return "."
        return grid[c]

    def get_left(self, grid):
        c = self.pos + self.dir_offsets[(4+self.dir-1)%4]
        if not c in grid:
            return "."
        return grid[c]

    def get_right(self, grid):
        c = self.pos + self.dir_offsets[(self.dir+1)%4]
        if not c in grid:
            return "."
        return grid[c]


def build_grid(output):
    x = 0
    y = 0
    robot = Robot()
    grid = {}
    for c in output:
        char = str(chr(c))
        if char == '\n':
            x = 0
            y += 1
        else:
            if char != '#' and char != ".":
                robot = Robot(Coord(x,y), char)
                char = '#'
            grid[Coord(x, y)] = char
            x += 1
    return grid, robot


def create_steps():
    text = open("input.txt").read().strip()
    computer = Computer(text)
    result, output = computer.run_program()
    grid, robot = build_grid(output)
    print_grid(grid, robot)

    steps = []
    current_step = 0
    for i in range(1000):
        if robot.get_front(grid) == '#':
            current_step += 1
            robot.move()
        else:
            if current_step!=0:
                steps.append(str(current_step))
            current_step = 0
            if robot.get_left(grid) == '#':
                robot.turn_left()
                steps.append("L")
            elif robot.get_right(grid) == '#':
                robot.turn_right()
                steps.append("R")
            else:
                break
    if current_step != 0:
        steps.append(str(current_step))
    return steps

def run_motion_program(motion_program):
    text = open("input.txt").read().strip()
    computer = Computer(text)
    computer.program[0] = 2
    for c in motion_program:
        computer.input.appendleft(ord(c))
    result, output = computer.run_program()
    computer.input.appendleft(ord('n'))
    computer.input.appendleft(ord('\n'))
    result, output = computer.run_program()
    print (''.join([str(chr(c)) for c in output]), output[-1], result)

def find_substring(steps, mincount, maxlen=20):
    text = ",".join(steps)
    for l in range(2, len(steps), 2):
        sequence = steps[0:-l]
        substring = ",".join(sequence)
        if len(substring)<=maxlen:
            #print (substring, text.count(substring))
            if text.count(substring) >= mincount:
                return text.count(substring), sequence
    return ""

def remove_sequence(steps, seq):
    nr = len(seq)
    i = 0
    result = []
    nr_found = 0
    while i+nr < len(steps):
        search = steps[i:i+nr]
        if steps[i:i+nr] == seq:
            i+= nr
            nr_found += 1
        else:
            result.append(steps[i])
            i+=1
    while i < len(steps):
        result.append(steps[i])
        i += 1
    return nr_found, result

if __name__ == "__main__":
    start_time = time.time()
    steps_a = create_steps()
    print (len(steps_a), ",".join(steps_a))
    nr, sequence_a = find_substring(steps_a, 3,19)
    print (nr, len(sequence_a), sequence_a)
    nr, steps_b = remove_sequence(steps_a, sequence_a)
    print (nr, len(steps_b), ",".join(steps_b))
    nr, sequence_b = find_substring(steps_b, 3)
    print (nr, len(sequence_b), sequence_b)
    nr, steps_c = remove_sequence(steps_b, sequence_b)
    print (nr, len(steps_c), ",".join(steps_c))
    nr, sequence_c = find_substring(steps_c, 3)
    print (nr, len(sequence_c), sequence_c)
    nr, steps_d = remove_sequence(steps_c, sequence_c)
    print (nr, steps_d)
    # manuell im notepadd zusammengebastelt ...
    #motion_program = """A,C,A,B,A,B,C,B,B,C\nL,4,L,4,L,10,R,4\nR,4,L,10,R,10\nR,4,L,4,L,4,R,8,R,10\n"""
    #run_motion_program(motion_program)
    print(time.time() - start_time)
