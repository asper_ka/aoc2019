# -*- coding: utf-8 -*-
import re
import time


class Deck:
    def __init__(self, size):
        self.size = size

    def get_card(self, n):
        return n

    def get_position(self, n):
        return n


class DealNew():
    def __init__(self, deck):
        self.size = deck.size
        self.deck = deck

    def get_card(self, n):
        idx = self.size - n - 1
        return self.deck.get_card(idx)

    def get_position(self, n):
        pos = self.size - n - 1
        return self.deck.get_position(pos)


class CutN():
    def __init__(self, deck, cut):
        self.size = deck.size
        self.deck = deck
        self.cut = cut

    def get_card(self, n):
        idx = (n + self.cut) % self.size
        return self.deck.get_card(idx)

class DealIncrement():
    def __init__(self, deck, incr):
        self.size = deck.size
        self.deck = deck
        self.incr = incr
        pos = 0
        card = 0
        #while pos != 1:
        #    card += 1
        #    pos = self.get_position(card)
        self.second_card = card

    def get_card(self, n):
        return self.deck.get_card((n*self.second_card) % self.size)

    def get_position(self, n):
        return (self.incr * n) % self.size

def create_shuffler_from_line(line, deck):
    if line == "deal into new stack":
        return DealNew(deck)
    match = re.match("cut (-?\d+)", line)
    if match:
        return CutN(deck, int(match.groups()[0]))
    match = re.match("deal with increment (-?\d+)", line)
    if match:
        return DealIncrement(deck, int(match.groups()[0]))


def create_shufflers(text, deck):
    shuffler = None
    for line in text.strip().split('\n'):
        shuffler = create_shuffler_from_line(line, deck)
        deck = shuffler
    return shuffler


def run_part_one():
    text = open("input.txt").read().strip()
    shuffler = create_shufflers(text, Deck(10007))
    shuffled = [shuffler.get_card(x) for x in range(10007)]
    print(shuffled.index(2019))

def run_part_two():
    text = open("input.txt").read().strip()
    shuffler = create_shufflers(text, Deck(119315717514047))
    pos = 2034
    for i in range (1000):
        pos = shuffler.get_card(pos)
        print (pos)


if __name__ == "__main__":
    start_time = time.time()
    run_part_two()
    print(time.time() - start_time)
