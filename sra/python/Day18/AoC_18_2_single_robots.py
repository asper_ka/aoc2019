# -*- coding: utf-8 -*-
import collections
import copy
import math
import time

text_input = """#######
#a.#Cd#
##...##
##.@.##
##...##
#cB#Ab#
#######"""

text_input2="""###############
#d.ABC.#.....a#
######...######
######.@.######
######...######
#b.....#.....c#
###############"""

text_input3="""#############
#DcBa.#.GhKl#
#.###...#I###
#e#d#.@.#j#k#
###C#...###J#
#fEbA.#.FgHi#
#############"""

text_input4="""#############
#g#f.D#..h#l#
#F###e#E###.#
#dCba...BcIJ#
#####.@.#####
#nK.L...G...#
#M###N#H###.#
#o#m..#i#jk.#
#############"""



class Grid():
    def __init__(self, text=""):
        self.keys = {}
        self.doors = {}
        self.grid = {}
        self.maxx = 0
        self.maxy = 0
        self.robot = Coord(0, 0)
        self.all_keys = 0
        self.dirs = {1: Coord(0, -1), 2: Coord(0, 1), 3: Coord(-1, 0), 4: Coord(1, 0)}
        self.opposite_dir = {0: 0, 1: 2, 2: 1, 3: 4, 4: 3}
        self.create_grid(text)

    def create_grid(self, text):
        lines = text.strip().split()
        if len(lines) == 0:
            return
        self.maxy = len(lines) - 1
        self.maxx = len(lines[0]) - 1
        for y in range(self.maxy + 1):
            for x in range(self.maxx + 1):
                c = Coord(x, y)
                char = lines[y][x]
                if char == "." or char == "#":
                    self.grid[c] = char
                else:
                    if char == "@":
                        self.robot = c
                        char = "."
                    elif char.islower():
                        keynr = ord(char) - ord('a')
                        self.keys[c] = pow(2,keynr)
                        self.all_keys += self.keys[c]
                        char = "."
                    else:
                        doornr = ord(char) - ord('A')
                        self.doors[c] = pow(2,doornr)
                        char = "."
                    self.grid[c] = char
        for d in self.dirs:
            self.grid[self.robot+self.dirs[d]] = "#"
        self.grid[self.robot] = "#"

    def __str__(self):
        text = ""
        for y in range(self.maxy + 1):
            for x in range(self.maxx + 1):
                c = Coord(x, y)
                if self.robot == c:
                    text += "@"
                elif c in self.keys:
                    text += str(chr(ord('a') + int(math.log2(self.keys[c]))))
                elif c in self.doors:
                    text += str(chr(ord('A') + int(math.log2(self.doors[c]))))
                else:
                    text += self.grid[c]
            text += "\n"
        return text

class Coord:
    def __init__(self, x, y):
        self.x = int(x)
        self.y = int(y)

    def __eq__(self, other):
        return self.x == other.x and self.y == other.y

    def __ne__(self, other):
        return not (self == other)

    def clone(self):
        return Coord(self.x, self.y)

    def __add__(self, other):
        return Coord(self.x + other.x, self.y + other.y)

    def __hash__(self):
        return hash((self.x, self.y))

    def __str__(self):
        return "({0}, {1})".format(self.x, self.y)

def find_neighbors(grid, pos, keys):
    neighbors = []
    for d in grid.dirs.keys():
        nb = pos + grid.dirs[d]
        if nb in grid.grid and grid.grid[nb] != '#':
            if nb in grid.doors:
                if keys & grid.doors[nb] != 0:
                    neighbors.append(nb)
            else:
                neighbors.append(nb)
    #print ("nb: ", ",".join([str(n) for n in neighbors]))
    return neighbors


def find(grid):
    queue = collections.deque()
    queue.append((grid.robot, 0, 0))
    visited = set()
    visited.add((grid.robot, 0))
    while len(queue)>0:
        pos, keys, steps = queue.popleft()
        # if (steps % 10)==0:
        #     print (pos, keys, steps)
        #print (pos, keys, steps, ",".join([str(n[0])+str(n[1]) for n in visited]))
        nbs = find_neighbors(grid, pos, keys)
        for nb in nbs:
            nkeys = keys
            if nb in grid.keys:
                nkeys = keys | grid.keys[nb]
            if (nb, nkeys) in visited:
                continue
            if nkeys == grid.all_keys:
                print (steps + 1)
                return steps + 1
            queue.append((nb, nkeys, steps + 1))
            visited.add((nb, nkeys))
    return 0

def find_keys_and_remove_doors(grid, cx, cy):
    grid.all_keys = 0
    keys_in_q = set()
    for (coord, key_value) in grid.keys.items():
        if in_same_quadrant(grid, coord, cx, cy):
            # add key to va
            grid.all_keys += key_value
            keys_in_q.add(key_value)
    # remove door
    grid.doors = {key: val for key, val in grid.doors.items() if val in keys_in_q}

def in_same_quadrant(grid, coord, cx, cy):
    dx = coord.x - grid.robot.x
    dy = coord.y - grid.robot.y
    return math.copysign(1, dx) == cx and math.copysign(1, dy) == cy


def run():
    text = open("input.txt").read()
    #text = text_input4
    grid = Grid(text)
    print(grid.robot)
    sum = 0
    for c in [Coord(-1,-1), Coord(-1,1), Coord(1,1), Coord(1,-1)]:
        g = copy.deepcopy(grid)
        find_keys_and_remove_doors(g, c.x, c.y)
        g.robot = g.robot + c
        print (g)
        sum += find(g)
    print (sum)

if __name__ == "__main__":
    start_time = time.time()
    run()
    print(time.time() - start_time)
