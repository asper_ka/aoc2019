from unittest import TestCase

from sra.python.Day14.AoC_14_2 import Reactor, Reaction

text = """10 ORE => 10 A
1 ORE => 1 B
7 A, 1 B => 1 C
7 A, 1 C => 1 D
7 A, 1 D => 1 E
7 A, 1 E => 1 FUEL"""




class Test(TestCase):
    def test_extract_chemical(self):
        reactor = Reactor()
        self.assertEqual((5, "FUEL"), reactor.extract_chemical("5 FUEL"), )

    def test_parse_line(self):
        reactor = Reactor()
        reactor.parse_line("7 A, 1 E => 1 FUEL")
        self.assertTrue("FUEL" in reactor.reactions)
        self.assertEqual("1 FUEL: 7*A 1*E", str(reactor.reactions["FUEL"]))

    def test_parse_input(self):
        reactor = Reactor()
        reactor.create_reactions(text)
        self.assertEqual("10 A: 10*ORE", str(reactor.reactions["A"]))
        self.assertEqual("1 B: 1*ORE", str(reactor.reactions["B"]))
        self.assertEqual("1 C: 7*A 1*B", str(reactor.reactions["C"]))
        self.assertEqual("1 D: 7*A 1*C", str(reactor.reactions["D"]))
        self.assertEqual("1 E: 7*A 1*D", str(reactor.reactions["E"]))
        self.assertEqual("1 FUEL: 7*A 1*E", str(reactor.reactions["FUEL"]))
        reactor.create_stock()
        self.assertDictEqual({"A":0, "B":0, "C":0, "D":0, "E":0, "FUEL":0, "ORE":0}, reactor.stock)

    def test_is_output_only(self):
        reactor = Reactor()
        reactor.create_reactions(text)
        self.assertTrue(reactor.is_output_only ("FUEL"))
        self.assertFalse(reactor.is_output_only ("E"))

    def test_combine_reactions(self):
        reaction = Reactor.combine_reaction (Reaction("FUEL", 1, {"A":7, "E":1}),
                                             Reaction("E", 1, {"A":7, "D":1}))
        self.assertEqual("1 FUEL: 14*A 1*D", str(reaction))

    def test_combine_reactions_2(self):
        reaction = Reactor.combine_reaction (Reaction("FUEL", 1, {"A":7, "E":2}),
                                             Reaction("E", 1, {"A":7, "D":1}))
        self.assertEqual("1 FUEL: 21*A 2*D", str(reaction))

    def test_combine_reactions_3(self):
        reaction = Reactor.combine_reaction (Reaction("FUEL", 1, {"A":7, "E":3}),
                                             Reaction("E", 2, {"A":7, "D":1}))
        self.assertEqual("1 FUEL: 21*A 2*D", str(reaction))

    def test_combine_reactions_all(self):
        reaction = Reactor.combine_reaction (Reaction("FUEL", 1, {"A":7, "E":1}),
                                             Reaction("E", 1, {"A":7, "D":1}))
        self.assertEqual("1 FUEL: 14*A 1*D", str(reaction))

        reaction = Reactor.combine_reaction (reaction,
                                             Reaction("D", 1, {"A":7, "C":1}))
        self.assertEqual("1 FUEL: 21*A 1*C", str(reaction))

        reaction = Reactor.combine_reaction (reaction,
                                             Reaction("C", 1, {"A":7, "B":1}))
        self.assertEqual("1 FUEL: 28*A 1*B", str(reaction))

        reaction = Reactor.combine_reaction (reaction,
                                             Reaction("A", 10, {"ORE":10}))
        self.assertEqual("1 FUEL: 1*B 30*ORE", str(reaction))

        reaction = Reactor.combine_reaction (reaction,
                                             Reaction("B", 1, {"ORE":1}))
        self.assertEqual("1 FUEL: 31*ORE", str(reaction))

    def test_produce_chemicals_one_step(self):
        reactor = Reactor()
        reactor.create_reactions("10 ORE => 10 A")
        reactor.create_stock()
        reactor.stock["ORE"] = 100
        reactor.produce_chemicals("A", 4)
        self.assertDictEqual({"A":10, "ORE":90}, reactor.stock)

        reactor.stock["ORE"] = 9
        self.assertRaises(Reactor.OutOfStock, reactor.produce_chemicals, 4, "A")

    def test_produce_chemicals_two_steps(self):
        reactor = Reactor()
        reactor.create_reactions("1 ORE => 1 B\n1 B, 30 ORE => 1 FUEL")
        reactor.create_stock()
        reactor.stock["ORE"] = 100
        reactor.produce_chemicals("FUEL", 1)
        self.assertDictEqual({"B":0, "FUEL": 1, "ORE": 69}, reactor.stock)

    def test_produce_chemicals_text_input(self):
        reactor = Reactor()
        reactor.create_reactions(text)
        reactor.create_stock()
        reactor.stock["ORE"] = 1000
        reactor.produce_chemicals("FUEL", 1)
        self.assertDictEqual({"A":2, "B":0, "C":0, "D":0, "E":0, "FUEL":1, "ORE":969}, reactor.stock)
        reactor.produce_chemicals("FUEL", 3)
        self.assertDictEqual({"A":8, "B":0, "C":0, "D":0, "E":0, "FUEL":4, "ORE":876}, reactor.stock)
        reactor.produce_chemicals("FUEL", 1)
        # next step only consumes 21 ORE, because we need 28 A to produce one FUEL
        self.assertDictEqual({"A":0, "B":0, "C":0, "D":0, "E":0, "FUEL":5, "ORE":855}, reactor.stock)

