# -*- coding: utf-8 -*-

import time

def parse_instruction(instruction):
    text = str(instruction)
    opcode = int(text[-2:])
    modes=[]
    for i in range(len(text)-3,-1,-1):
        modes.append(int(text[i]))
    for i in range(len(modes),3):
        modes.append(0)
    return opcode, modes

def get_value(param, mode, program):
    if (mode==0):
        return program[param]
    else:
        return param

def create_program(text):
    return [ int(x) for x in text.split(",") ]

def run_program (program, input):
    output=[]
    ip = 0 # instruction pointer
    while True:
        opcode, modes = parse_instruction(program[ip])
        if opcode==99:
            return True, output
        if opcode==1:
            params = program[ip + 1: ip + 4]
            program[params[2]]=get_value(params[0], modes[0], program)+get_value(params[1], modes[1], program)
            ip+=4
        elif opcode == 2:
            params = program[ip + 1: ip + 4]
            program[params[2]] = get_value(params[0], modes[0], program) * get_value(params[1], modes[1], program)
            ip += 4
        elif opcode == 3:
            param = program[ip + 1]
            program[param] = input
            ip += 2
        elif opcode == 4:
            param = program[ip + 1]
            output.append(get_value(param, modes[0], program))
            ip += 2
        elif opcode == 5:
            params = program[ip + 1:ip + 3]
            if get_value(params[0], modes[0], program) != 0:
                ip = get_value(params[1], modes[1], program)
            else:
                ip += 3
        elif opcode == 6:
            params = program[ip + 1:ip + 3]
            if get_value(params[0], modes[0], program) == 0:
                ip = get_value(params[1], modes[1], program)
            else:
                ip += 3
        elif opcode == 7:
            params = program[ip + 1:ip + 4]
            if get_value(params[0], modes[0], program) < get_value(params[1], modes[1], program):
                program[params[2]] = 1
            else:
                program[params[2]] = 0
            ip += 4
        elif opcode == 8:
            params = program[ip + 1:ip + 4]
            if get_value(params[0], modes[0], program) == get_value(params[1], modes[1], program):
                program[params[2]] = 1
            else:
                program[params[2]] = 0
            ip += 4

        else:
            print("ERROR")
            return False, output

def run():
    text = open("input.txt").read()
    (result, output) = run_program(create_program(text), 1)
    if not result:
        print ("FAILED")
    else:
        print (output)

if __name__ == "__main__":
    start_time = time.time()
    run()
    print (time.time() - start_time)