# -*- coding: utf-8 -*-
import operator
import time
from collections import deque
from enum import Enum

from sra.python.Computer import Computer


def get_color(panels, coord):
    if not coord in panels:
        return 0
    return panels[coord]

class Direction(Enum):
    Top = 1
    Left = 2
    Down = 3
    Right = 4

def move(coord, dir):
    if dir == Direction.Top:
        return (coord[0], coord[1]-1)
    if dir == Direction.Left:
        return (coord[0]-1, coord[1])
    if dir == Direction.Down:
        return (coord[0], coord[1]+1)
    if dir == Direction.Right:
        return (coord[0]+1, coord[1])

def rotate(rotation, dir):
    if dir == Direction.Top:
        if rotation == 0: #left
            return Direction.Left
        if rotation == 1:
            return Direction.Right
    if dir == Direction.Left:
        if rotation == 0: #left
            return Direction.Down
        if rotation == 1:
            return Direction.Top
    if dir == Direction.Down:
        if rotation == 0: #left
            return Direction.Right
        if rotation == 1:
            return Direction.Left
    if dir == Direction.Right:
        if rotation == 0: #left
            return Direction.Top
        if rotation == 1:
            return Direction.Down

def run():
    text = open("input.txt").read()
    computer = Computer(text)
    #panels = {(0,0):0} # part one
    panels = {(0,0):1} # part two
    robot = (0,0)
    dir = Direction.Top
    result = 0
    while True:
        computer.input.appendleft(get_color(panels, robot))
        result, output = computer.run_program(2)
        if result != Computer.State.Output:
            break
        panels[robot] = output[0]
        dir = rotate(output[1], dir)
        robot = move(robot, dir)

    #print (result, len(panels)) # result for part one

    minx = min(panels, key=operator.itemgetter(0))[0]
    maxx = max(panels, key=operator.itemgetter(0))[0]
    miny = min(panels, key=operator.itemgetter(1))[1]
    maxy = max(panels, key=operator.itemgetter(1))[1]

    for y in range (miny, maxy+1):
        for x in range(minx, maxx + 1):
            c = "  "
            if (x,y) in panels:
                if panels[(x,y)] == 1:
                    c = "##"
            print (c, end="")
        print()





if __name__ == "__main__":
    start_time = time.time()
    run()
    print(time.time() - start_time)
