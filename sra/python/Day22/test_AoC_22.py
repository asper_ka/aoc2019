from unittest import TestCase

from sra.python.Day22.AoC_22 import Deck, DealNew, CutN, DealIncrement, create_shuffler_from_line, create_shufflers

test_input1="""deal into new stack
cut -2
deal with increment 7
cut 8
cut -4
deal with increment 7
cut 3
deal with increment 9
deal with increment 3
cut -1
"""




class TestDeck(TestCase):
    def test_get_card(self):
        deck = Deck(10)
        self.assertEqual(deck.get_card(3), 3)
        self.assertEqual(deck.get_card(6), 6)

    def test_deal_new(self):
        deck = Deck(10)
        deal_new = DealNew(deck)
        shuffled = [deal_new.get_card(x) for x in range(10)]
        self.assertListEqual([9, 8, 7, 6, 5, 4, 3, 2, 1, 0], shuffled)
        self.assertEqual(2, deal_new.get_position((7)))
        self.assertEqual(8, deal_new.get_position((1)))

    def test_cut_n(self):
        deck = Deck(10)
        cut_3 = CutN(deck, 3)
        shuffled = [cut_3.get_card(x) for x in range(10)]
        self.assertListEqual([3, 4, 5, 6, 7, 8, 9, 0, 1, 2], shuffled)

        cut_minus_4 = CutN(deck, -4)
        shuffled = [cut_minus_4.get_card(x) for x in range(10)]
        self.assertListEqual([6, 7, 8, 9, 0, 1, 2, 3, 4, 5], shuffled)

    def test_deal_increment(self):
        deck = Deck(10)
        deal_incr_3 = DealIncrement(deck, 3)
        #shuffled = [deal_incr_3.get_card(x) for x in range(10)]
        #self.assertListEqual([0, 7, 4, 1, 8, 5, 2, 9, 6, 3], shuffled)
        self.assertEqual(0, deal_incr_3.get_position(0))
        self.assertEqual(1, deal_incr_3.get_position(7))
        self.assertEqual(9, deal_incr_3.get_position(3))
        self.assertEqual(0, deal_incr_3.get_card(0))
        self.assertEqual(7, deal_incr_3.get_card(1))

        deal_incr_7 = DealIncrement(deck, 7)
        shuffled = [deal_incr_7.get_card(x) for x in range(10)]
        self.assertListEqual([0,3,6,9,2,5,8,1,4,7], shuffled)

        deck11 = Deck(11)
        deal_incr_5 = DealIncrement(deck11, 5)
        shuffled = [deal_incr_5.get_card(x) for x in range(11)]
        self.assertListEqual([0, 9, 7, 5, 3, 1, 10, 8, 6, 4, 2 ], shuffled)

        deck11 = Deck(11)
        deal_incr_4 = DealIncrement(deck11, 4)
        shuffled = [deal_incr_4.get_card(x) for x in range(11)]
        self.assertListEqual([0, 3, 6, 9, 1, 4, 7, 10, 2, 5, 8 ], shuffled)
        self.assertEqual(3, deal_incr_4.get_card(1))
        self.assertEqual(4, deal_incr_4.get_card(5))

        deck13 = Deck(13)
        deal_incr_5 = DealIncrement(deck13, 2)
        shuffled = [deal_incr_5.get_card(x) for x in range(13)]
        self.assertListEqual([0, 7, 1, 8, 2, 9, 3, 10, 4, 11, 5, 12, 6 ], shuffled)

    def test_sequence1(self):
        deck = Deck(10)
        shuffler1 = DealIncrement(deck, 7)
        shuffler2 = DealNew(shuffler1)
        shuffler3 = DealNew(shuffler2)
        shuffled = [shuffler3.get_card(x) for x in range(10)]
        self.assertListEqual([0, 3, 6, 9, 2, 5, 8, 1, 4, 7], shuffled)

    def test_sequence2(self):
        deck = Deck(10)
        shuffler1 = DealNew(deck)
        shuffler2 = CutN(shuffler1, -2)
        shuffler3 = DealIncrement(shuffler2, 7)
        shuffler4 = CutN(shuffler3, 8)
        shuffler5 = CutN(shuffler4, -4)
        shuffler6 = DealIncrement(shuffler5, 7)
        shuffler7 = CutN(shuffler6, 3)
        shuffler8 = DealIncrement(shuffler7, 9)
        shuffler9 = DealIncrement(shuffler8, 3)
        shuffler10 = CutN(shuffler9, -1)
        shuffled = [shuffler10.get_card(x) for x in range(10)]
        self.assertListEqual([9, 2, 5, 8, 1, 4, 7, 0, 3, 6], shuffled)

    def test_create_shuffler_from_line_cut(self):
        deck = Deck(10)
        shuffler = create_shuffler_from_line ("cut -1", deck)
        self.assertTrue(isinstance(shuffler, CutN))
        self.assertEqual(-1, shuffler.cut)
        shuffler = create_shuffler_from_line ("cut -234", deck)
        self.assertTrue(isinstance(shuffler, CutN))
        self.assertEqual(-234, shuffler.cut)
        shuffler = create_shuffler_from_line ("cut 4567", deck)
        self.assertTrue(isinstance(shuffler, CutN))
        self.assertEqual(4567, shuffler.cut)

    def test_create_shuffler_from_line_deal_new(self):
        deck = Deck(10)
        shuffler = create_shuffler_from_line("deal into new stack", deck)
        self.assertTrue(isinstance(shuffler, DealNew))

    def test_create_shuffler_from_line_deal_incr(self):
        deck = Deck(10)
        shuffler = create_shuffler_from_line("deal with increment 7", deck)
        self.assertTrue(isinstance(shuffler, DealIncrement))
        self.assertEqual(7, shuffler.incr)

    def test_create_shufflers(self):
        deck = Deck(10)
        shuffler = create_shufflers(test_input1, deck)
        shuffled = [shuffler.get_card(x) for x in range(10)]
        self.assertListEqual([9, 2, 5, 8, 1, 4, 7, 0, 3, 6], shuffled)
