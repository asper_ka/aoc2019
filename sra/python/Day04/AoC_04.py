# -*- coding: utf-8 -*-

import time

def is_not_decreasing(t):
    for i in range(len(t)-1):
        if int(t[i+1])<int(t[i]):
            return False
    return True

def two_adjacent_same_digits(t):
    one_found = False
    for i in range(len(t)-1):
        if int(t[i+1])==int(t[i]):
            return True
    return False

def has_pair(t):
    i=0
    while i <(len(t)-1):
        n = 1
        i2 = i+1
        while i2<len(t) and t[i2]==t[i]:
            n+=1
            i2+=1
        if (n==2):
            return True
        i = i2
    return False

if __name__ == "__main__":
    start_time = time.time()
    count_part1 = 0
    count_part2 = 0
    for n in range (153517,630396):
        text=str(n)
        if (is_not_decreasing(text) and two_adjacent_same_digits(text)):
            count_part1 += 1
            if has_pair(text):
                count_part2 += 1

    print (count_part1)
    print (count_part2)

    print (time.time() - start_time)