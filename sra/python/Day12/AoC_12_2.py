# -*- coding: utf-8 -*-
import itertools
import time
import re

from functools import reduce
from math import gcd

def lcm(a, b):
    return int(a * b / gcd(a, b))

def lcms(*numbers):
    return reduce(lcm, numbers)

test_input = """<x=-1, y=0, z=2>
<x=2, y=-10, z=-7>
<x=4, y=-8, z=8>
<x=3, y=5, z=-1>"""

test_input2 = """<x=-8, y=-10, z=0>
<x=5, y=5, z=10>
<x=2, y=-7, z=3>
<x=9, y=-8, z=-3>"""


puzzle_input="""<x=-7, y=-8, z=9>
<x=-12, y=-3, z=-4>
<x=6, y=-17, z=-9>
<x=4, y=-10, z=-6>"""

def parse_line(line):
    m = re.search("<x=(-?\d+), y=(-?\d+), z=(-?\d+)>",line)
    if m:
        return (int(m.group(1)),int(m.group(2)),int(m.group(3)))
    return (0,0,0)

def parse_input(text):
    planets = []
    for l in text.split("\n"):
        print (l)
        planets.append(parse_line(l))
    return planets

def update_vel(i1, i2, planets, vels):
    c1 = planets[i1]
    c2 = planets[i2]
    v1 = []
    v2 = []
    for i in range(3):
        if c1[i] > c2[i]:
            v1.append( vels[i1][i] - 1)
            v2.append( vels[i2][i] + 1)
        elif c1[i] < c2[i]:
            v1.append(vels[i1][i] + 1)
            v2.append(vels[i2][i] - 1)
        else:
            v1.append(vels[i1][i])
            v2.append(vels[i2][i])
    vels[i1] = (v1[0], v1[1], v1[2] )
    vels[i2] = (v2[0], v2[1], v2[2] )


def get_new_pos(planet, vel):
    return (planet[0] + vel[0], planet[1] + vel[1], planet[2] + vel[2])


def run():
    text = puzzle_input
    #text = test_input2
    planets0 = parse_input(text)
    planets = parse_input(text)
    vels = []
    history = {}
    periods ={0:0, 1:0, 2:0}
    for p in planets:
        vels.append((0,0,0))
    for nr in range (1000000):
        for p in itertools.combinations(range(len(planets)), 2):
            update_vel(p[0], p[1], planets, vels)
        planets2 = []
        for i in range(len(planets)):
            planets2.append(get_new_pos(planets[i], vels[i]))
        #print()
        planets = planets2
        for c in range(3):
            period = True
            for i in range(len(planets)):
                if vels[i][c]!=0:
                    period = False
                if planets[i][c]!=planets0[i][c]:
                    period = False
            if period and periods[c]==0:
                periods[c] = nr+1
    print (periods)
    print (lcms(periods[0], periods[1], periods[2]))

if __name__ == "__main__":
    start_time = time.time()
    run()
    print(time.time() - start_time)
