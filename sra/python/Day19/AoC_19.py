# -*- coding: utf-8 -*-

import time
from sra.python.Computer import Computer


def print_grid(grid):
    minx = min([c[0] for c in grid.keys()])
    miny = min([c[1] for c in grid.keys()])
    maxx = max([c[0] for c in grid.keys()])
    maxy = max([c[1] for c in grid.keys()])
    for y in range(miny, maxy + 1):
        line = ""
        for x in range(minx, maxx + 1):
            if (x, y) in grid:
                line += grid[(x, y)]
            else:
                line += ' '
        print(line)


def run1():
    grid = create_grid(0, 50)
    print_grid(grid)
    print(list(grid.values()).count('#') + 1)


def create_grid(start, end):
    text = open("input.txt").read().strip()
    grid = {}
    sum = 0
    last_x = 0
    last_width = 0
    for y in range(start, end):
        x = max(last_x, int(y / 3))
        while x < y:
            computer = Computer(text)
            computer.input.appendleft(x)
            computer.input.appendleft(y)
            result, output = computer.run_program()
            if output[0] == 1:
                grid[(x, y)] = '#'
                last_x = x
                break;
            else:
                grid[(x, y)] = '.'
            x += 1
        if last_width > 0:
            for i in range(last_width):
                grid[(x, y)] = '#'
                x += 1
        while x < y:
            computer = Computer(text)
            computer.input.appendleft(x)
            computer.input.appendleft(y)
            result, output = computer.run_program()
            if output[0] == 1:
                grid[(x, y)] = '#'
            else:
                grid[(x, y)] = '.'
                last_width = x - last_x - 1
                break
            x += 1
    return grid


def get_grid(grid, x, y):
    if (x, y) in grid:
        return grid[(x, y)]
    return '.'


def run2():
    y1 = 100
    y2 = 2000
    grid = create_grid(y1, y2)
    for y in range(y1, y2):
        x = int(y / 3)
        while x < y:
            if get_grid(grid, x, y) == '#' and get_grid(grid, x + 99, y) == '#' and\
                    get_grid(grid, x,y + 99) == '#' and get_grid(grid, x + 99, y + 99) == '#':
                print(x, y)
                return
            x += 1


if __name__ == "__main__":
    start_time = time.time()
    run2()
    print(time.time() - start_time)
