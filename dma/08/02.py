#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np

data = []
for c in open('input.txt').read().strip():
    data.append(int(c))

data = np.array(data)
width = 25
height = 6
num_layers = int(len(data) / (width*height))
data = data.reshape(num_layers, width*height)

result = np.empty(shape=(height, width))

for row in range(height):
    for col in range(width):
        for layer in range(num_layers):
            pixel = data[layer, row*width+col]
            if pixel in (0, 1):
                result[row, col] = pixel
                break

for row in result:
    for col in row:
        if col == 0:
            print(' ', end='')
        else:
            print('*', end='')
    print()
