#!/usr/bin/env python
# -*- coding: utf-8 -*-

import networkx as nx

orbits = nx.Graph()

for line in open('input.txt'):
    center, orbiter = line.strip().split(')')
    orbits.add_node(orbiter)
    orbits.add_edge(orbiter, center)

result = nx.shortest_path_length(orbits, 'YOU', 'SAN')
print(result-2)
