from unittest import TestCase

from sra.python.Day10.AoC_10_2 import farey_sequence, Coord


class Test(TestCase):
    def test_farey_sequence(self):
        seq = farey_sequence(1)
        self.assertListEqual(seq, [Coord(0,1), Coord(1,1)])

        seq = farey_sequence(2)
        self.assertListEqual(seq, [Coord(0,1), Coord(1,2), Coord(1,1)])

        seq = farey_sequence(3)
        self.assertListEqual(seq, [Coord(0,1), Coord(1,3), Coord(1,2), Coord(2,3), Coord(1,1)])

        seq = farey_sequence(4)
        self.assertListEqual(seq, [Coord(0,1), Coord(1,4), Coord(1,3), Coord(1,2), Coord(2,3), Coord(3,4), Coord(1,1)])
