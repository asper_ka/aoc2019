# -*- coding: utf-8 -*-

import time

#text = """14
#1969
#100756"""
text = open("input.txt").read()

start_time = time.time()

sum = 0


def calc_fuel(mass):
    fuel = mass
    total_fuel: int = 0
    while True:
        fuel = int(fuel / 3) - 2
        if fuel <= 0:
            # print (total_fuel)
            return total_fuel
        total_fuel += fuel


for l in text.split():
    mass = int(l)
    sum += calc_fuel(mass)

print (sum)
print (time.time() - start_time)