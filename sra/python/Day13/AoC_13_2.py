# -*- coding: utf-8 -*-
import operator
import time
from collections import deque
from enum import Enum

from sra.python.Computer import Computer


class GameDisplay:
    def __init__(self):
        self.tiles = {}
        self.ballx = 0
        self.paddlex = 0
        self.score = 0
        self.maxx = 37
        self.maxy = 20
        self.tilechars = {0:' ', 1: '#', 2: '+', 3:'-', 4:'*'}

    def print_tiles(self):
        for y in range(self.maxy+1):
            line = ""
            for x in range(self.maxx+1):
                if not (x,y) in self.tiles:
                    line += " "
                else:
                    line += self.tilechars[self.tiles[(x,y)]]
            print (line)

    def update_display(self, output):
        for i in range(0, len(output), 3):
            if output[i]==-1 and output[i+1]==0:
                self.score = output[i+2]
            else:
                self.tiles[(output[i], output[i + 1])] = output[i + 2]
                if output[i + 2] == 4:
                    self.ballx = output[i]
                if output[i + 2] == 3:
                    self.paddlex = output[i]
        #print_tiles(tiles, 37, 20)
        #print ("Score: [  {}  ]".format(score))

def run():
    text = open("input.txt").read()
    computer = Computer(text)
    computer.program[0] = 2
    display = GameDisplay()
    while (True):
        result , output = computer.run_program()
        #print (result, output)
        display.update_display(output)
        input = 0
        if display.ballx > display.paddlex:
            input = 1
        if display.ballx < display.paddlex:
            input = -1
        #print (input, ballx, paddlex)
        computer.input.appendleft(input)
        if result == Computer.State.Halt:
            break
    print (display.score)


if __name__ == "__main__":
    start_time = time.time()
    run()
    print(time.time() - start_time)
