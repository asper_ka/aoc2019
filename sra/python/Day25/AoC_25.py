# -*- coding: utf-8 -*-

import time
import sys
sys.path.append("..\..\..")
from sra.python.Computer import Computer
test_room ="""


== Hull Breach ==
You got in through a hole in the floor here. To keep your ship from also freezing, the hole has been sealed.

Doors here lead:
- north
- south
- west

Command?
"""

test_room2 = """


== Stables ==
Reindeer-sized. They're all empty.

Doors here lead:
- north
- east
- south

Items here:
- escape pod

Command?
"""

def parse_room(text):
    name = ""
    dirs = []
    items = []
    lines = text.split('\n')
    idx = 0
    while lines[idx] == "":
        idx += 1
    name = lines[idx]
    while idx < len(lines) and lines[idx] != "Doors here lead:":
        idx += 1
    if idx < len(lines) and lines[idx] == "Doors here lead:":
        idx += 1
        while lines[idx] != "":
            dirs.append(lines[idx][2:])
            idx += 1
    while idx < len(lines) and lines[idx] != "Items here:":
        idx += 1
    if idx < len(lines) and lines[idx] == "Items here:":
        idx += 1
        while lines[idx] != "":
            items.append(lines[idx][2:])
            idx += 1
        
    return name, dirs, items
    
class Droid:
    def __init__(self):
        self.dirs = ["north", "west", "south", "east"]
        self.current_dir = 0
        
    def move(self, dirs):
        print ("move ", dirs, " - current dir:",self.dirs[self.current_dir])
        dir = (self.current_dir + 1) % 4
        if self.dirs[dir] in dirs: # rechts abbiegen?
            self.current_dir = dir
        elif not self.dirs[self.current_dir] in dirs: # geradeaus weiter nicht möglich
            dir = (self.current_dir - 1) % 4
            while not self.dirs[dir] in dirs:
                dir = (dir - 1) % 4
            self.current_dir = dir
        return self.dirs[self.current_dir]

def send_to_computer(computer, text):
    for c in text:
        computer.input.appendleft(ord(c))

def collect_all_items(computer, droid):
    inventory = []
    while len(inventory) < 8:
        result, output = computer.run_program()
        text = ''.join([str(chr(x)) for x in output])
        if result != Computer.State.WaitInput:
            print ("ERROR", text)
            break
        name, dirs, items = parse_room(text)
        if name == "== Security Checkpoint ==" and "west" in dirs:
            dirs.remove("west")
        print (name, dirs, items)
        for i in items:
            if not i in ["photons", "escape pod", "giant electromagnet", "infinite loop", "molten lava"]:
                cmd = "take "+i
                print (cmd)
                send_to_computer(computer, cmd +"\n")
                result, output = computer.run_program()
                text = ''.join([str(chr(x)) for x in output])
                if result != Computer.State.WaitInput:
                    print ("ERROR", text)
                    break
                inventory.append(i)
                if len(inventory) == 8:
                    print ("found all")
                    return inventory, dirs
                
        command = droid.move(dirs)
        print (command)
        send_to_computer(computer, command+"\n")
        
def goto_security_checkpoint(computer, droid, dirs):
    print ("goto_security_checkpoint", dirs)
    command = droid.move(dirs)
    print (command)
    send_to_computer(computer, command+"\n")
    while True:
        result, output = computer.run_program()
        text = ''.join([str(chr(x)) for x in output])
        if result != Computer.State.WaitInput:
            print ("ERROR", text)
            break
        name, dirs, items = parse_room(text)
        print (name, dirs, items)
        if name == "== Security Checkpoint ==":
            return
        command = droid.move(dirs)
        print (command)
        send_to_computer(computer, command+"\n")

def drop(computer, inventory, mask):
    for idx in range(len(inventory)):
        if mask & int(pow(2,idx)) != 0:
            command = "drop "+inventory[idx]
            print (command)
            send_to_computer(computer, command+"\n")
            result, output = computer.run_program()
            text = ''.join([str(chr(x)) for x in output])
            if result != Computer.State.WaitInput:
                print ("ERROR", text)
                return
    send_to_computer(computer, "inv\n")
    result, output = computer.run_program()
    text = ''.join([str(chr(x)) for x in output])
    print (text)

def try_enter(computer, inventory, mask):
    print (mask)
    for idx in range(len(inventory)):
        if mask & int(pow(2,idx)) != 0:
            command = "take "+inventory[idx]
            print (command)
            send_to_computer(computer, command+"\n")
            result, output = computer.run_program()
            text = ''.join([str(chr(x)) for x in output])
            if result != Computer.State.WaitInput:
                print ("ERROR", text)
                return
    print ("west")
    send_to_computer(computer, "west"+"\n")
    result, output = computer.run_program()
    text = ''.join([str(chr(x)) for x in output])
    print (result, text)
    return result

def run():
    text = open("input.txt").read().strip()
    computer = Computer(text)
    droid = Droid()
    inventory, dirs = collect_all_items(computer, droid)
    send_to_computer(computer, "inv\n")
    result, output = computer.run_program()
    text = ''.join([str(chr(x)) for x in output])
    print (text)
    print (inventory)
    goto_security_checkpoint(computer, droid, dirs)
    drop(computer, inventory, 255)
    for mask in range(1, 256):
        if try_enter(computer, inventory, mask) != Computer.State.WaitInput:
            break
        drop(computer, inventory, mask)

if __name__ == "__main__":
    start_time = time.time()
    run()
    print(time.time() - start_time)

#photons
#lava
#escape pod
#electromagnet