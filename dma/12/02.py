#!/usr/bin/env python
# -*- coding: utf-8 -*-

import itertools
import functools
import numpy as np


class Coord(object):
    def __init__(self, x=0, y=0, z=0):
        self.x = x
        self.y = y
        self.z = z

    def __eq__(self, other):
        return self.x == other.x and self.y == other.y and self.z == other.z


class Moon(object):
    def __init__(self, x, y, z, n):
        self.n = n
        self.pos = Coord(x, y, z)
        self.vel = Coord()

    def __repr__(self):
        return 'Moon {} (pos={},{},{}, vel={},{},{})'.format(
            self.n,
            self.pos.x,
            self.pos.y,
            self.pos.z,
            self.vel.x,
            self.vel.y,
            self.vel.z)

    def __eq__(self, other):
        return self.pos == other.pos and self.vel == other.vel

    def apply_gravity(self, other):
        if self.pos.x < other.pos.x:
            self.vel.x += 1
            other.vel.x -= 1
        elif self.pos.x > other.pos.x:
            self.vel.x -= 1
            other.vel.x += 1

        if self.pos.y < other.pos.y:
            self.vel.y += 1
            other.vel.y -= 1
        elif self.pos.y > other.pos.y:
            self.vel.y -= 1
            other.vel.y += 1

        if self.pos.z < other.pos.z:
            self.vel.z += 1
            other.vel.z -= 1
        elif self.pos.z > other.pos.z:
            self.vel.z -= 1
            other.vel.z += 1

    def apply_velocity(self):
        self.pos.x += self.vel.x
        self.pos.y += self.vel.y
        self.pos.z += self.vel.z

    def get_pot_energy(self):
        return abs(self.pos.x) + abs(self.pos.y) + abs(self.pos.z)

    def get_kin_energy(self):
        return abs(self.vel.x) + abs(self.vel.y) + abs(self.vel.z)

    def get_energy(self):
        return self.get_pot_energy() * self.get_kin_energy()


def load_moons():
    filename = 'input.txt'
    #filename = 'testinput.txt'

    moons = []
    for line in open(filename).readlines():
        items = line.strip()[1:-1].split(', ')
        x, y, z = [int(item.split('=')[1]) for item in items]
        n = len(moons)
        moons.append(Moon(x, y, z, n))

    return moons


def find_period(data):
    period = 2
    data = np.array(data)
    while period < len(data)/2:
        s1 = data[:period]
        s2 = data[period:2*period]
        if np.all(s1 == s2):
            return period
        period += 1
    raise ValueError('period not found')


#####################################################################
# https://stackoverflow.com/a/147539
def gcd(a, b):
    """Return greatest common divisor using Euclid's Algorithm."""
    while b:
        a, b = b, a % b
    return a


def lcm(a, b):
    """Return lowest common multiple."""
    return a * b // gcd(a, b)


def lcmm(numbers):
    """Return lcm of args."""
    return functools.reduce(lcm, numbers)
#####################################################################


def main():
    moons = load_moons()

    num_iter = int(1e6)
    data = np.empty(shape=(num_iter, len(moons)*6), dtype=np.int16)
    for i in range(num_iter):
        if i%1e4 == 0:
            print('Iteration {}'.format(i))
        data[i] = [moon.pos.x for moon in moons] + \
                  [moon.pos.y for moon in moons] + \
                  [moon.pos.z for moon in moons] + \
                  [moon.vel.x for moon in moons] + \
                  [moon.vel.y for moon in moons] + \
                  [moon.vel.z for moon in moons]

        for moon1, moon2 in itertools.combinations(moons, 2):
            moon1.apply_gravity(moon2)

        for moon in moons:
            moon.apply_velocity()

    data = data.T
    periods = [find_period(row) for row in data]
    print(periods)
    print(lcmm(set(periods)))
    #import matplotlib.pyplot as plt
    #for row in data:
    #    plt.plot(row, '-')
    #plt.show()


if __name__ == '__main__':
    main()
