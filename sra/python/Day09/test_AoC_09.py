from unittest import TestCase

from sra.python.Day09.AoC_09 import Computer


class Test(TestCase):
    def test_run_realtive_base(self):
        computer = Computer("109,19,204,-34,99")
        computer.relative_base = 1000
        computer.program[985] = 42
        result, output = computer.run_program()
        self.assertEqual(result, Computer.State.Halt)
        self.assertEqual(output, [42])

    def test_run_output_itself(self):
        computer = Computer("109,1,204,-1,1001,100,1,100,1008,100,16,101,1006,101,0,99")
        result, output = computer.run_program()
        self.assertEqual(result, Computer.State.Halt)
        self.assertEqual(output, [109,1,204,-1,1001,100,1,100,1008,100,16,101,1006,101,0,99])

    def test_run_output_16digit(self):
        computer = Computer("1102,34915192,34915192,7,4,7,99,0")
        result, output = computer.run_program()
        self.assertEqual(result, Computer.State.Halt)
        self.assertEqual(output, [1219070632396864])

    def test_run_output_16digit(self):
        computer = Computer("104,1125899906842624,99")
        result, output = computer.run_program()
        self.assertEqual(result, Computer.State.Halt)
        self.assertEqual(output, [1125899906842624])
