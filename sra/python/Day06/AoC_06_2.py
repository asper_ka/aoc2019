# -*- coding: utf-8 -*-

import time
import networkx as nx

test_input = """COM)B
B)C
C)D
D)E
E)F
B)G
G)H
D)I
E)J
J)K
K)L
K)YOU
I)SAN"""

def get_objects(text):
    list = text.split(")")
    return list[0], list[1]


def parse_input(text):
    g = nx.Graph()
    objects = {}
    for line in text.split():
        g.add_edge(*get_objects(line))
    return g

def run():
    text = open("input.txt").read()
    #text = test_input
    g = parse_input(text)
    path = nx.dijkstra_path(g, 'SAN', 'YOU')
    #print (path)
    print(len(path)-3)


if __name__ == "__main__":
    start_time = time.time()
    run()
    print(time.time() - start_time)
