from unittest import TestCase

from sra.python.Day16.AoC_16 import create_pattern, FactorGenerator


class Test(TestCase):
    def test_generate_factor_1(self):
        fg = FactorGenerator(1)
        self.assertEqual(1, fg.get_next())
        self.assertEqual(0, fg.get_next())
        self.assertEqual(-1, fg.get_next())
        self.assertEqual(0, fg.get_next())
        self.assertEqual(1, fg.get_next())
        self.assertEqual(0, fg.get_next())
        self.assertEqual(-1, fg.get_next())
        self.assertEqual(0, fg.get_next())

    def test_generate_factor_3(self):
        fg = FactorGenerator(3)
        self.assertEqual(0, fg.get_next())
        self.assertEqual(0, fg.get_next())
        self.assertEqual(1, fg.get_next())
        self.assertEqual(1, fg.get_next())
        self.assertEqual(1, fg.get_next())
        self.assertEqual(0, fg.get_next())
        self.assertEqual(0, fg.get_next())
        self.assertEqual(0, fg.get_next())
        self.assertEqual(-1, fg.get_next())
        self.assertEqual(-1, fg.get_next())
        self.assertEqual(-1, fg.get_next())
        self.assertEqual(0, fg.get_next())
        self.assertEqual(0, fg.get_next())
        self.assertEqual(0, fg.get_next())
