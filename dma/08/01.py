#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np

data = []
for c in open('input.txt').read().strip():
    data.append(int(c))

data = np.array(data)
width = 25
height = 6
num_layers = int(len(data) / (width*height))
data = data.reshape(num_layers, width*height)

num_zeros = []
for layer in data:
    num_zeros.append(len(np.where(layer==0)[0]))

fewest_zeros_layer = data[np.argmin(num_zeros)]
result = len(np.where(fewest_zeros_layer==1)[0]) * len(np.where(fewest_zeros_layer==2)[0])

print(result)
