# -*- coding: utf-8 -*-

import time

text = """R8,U5,L5,D3
U7,R6,D4,L4"""
text="""R75,D30,R83,U83,L12,D49,R71,U7,L72
U62,R66,U55,R34,D71,R55,D58,R83"""
text="""R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51
U98,R91,D20,R16,D67,R40,U7,R15,U6,R7"""

text = open("input.txt").read()

start_time = time.time()

def calc_dist(x,y):
    return abs(x)+abs(y)

def add_to_grid(x,y,c):
    grid[x,y]=c

def get_deltas(dir):
    if d=="U":
        return (0,1)
    elif d == "D":
        return (0, -1)
    elif d=="R":
        return (1,0)
    elif d=="L":
        return (-1,0)


intersections = []
grid = {}
color = 0
for l in text.split():
    color += 1
    dirs = l.split(",")
    x=0
    y=0
    for dir in dirs:
        add_to_grid(0, 0,color)
        d = dir[0]
        l = int(dir[1:])
        (dx,dy) = get_deltas(d)
        for i in range(l):
            x+=dx
            y+=dy
            if ((x,y) in grid) and (grid[(x,y)]!=color):
                intersections.append((x, y))
            add_to_grid(x,y,color)

distances={}

for l in text.split():
    dirs = l.split(",")
    x=0
    y=0
    steps = 0
    for dir in dirs:
        add_to_grid(0, 0,color)
        d = dir[0]
        l = int(dir[1:])
        (dx,dy) = get_deltas(d)
        for i in range(l):
            x+=dx
            y+=dy
            steps+=1
            if ((x,y) in intersections):
                if ((x,y) in distances):
                    distances[(x,y)]+=steps
                else:
                    distances[(x, y)]=steps

print (min(distances.values()))

print (time.time() - start_time)