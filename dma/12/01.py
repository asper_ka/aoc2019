#!/usr/bin/env python
# -*- coding: utf-8 -*-

import itertools


class Coord(object):
    def __init__(self, x=0, y=0, z=0):
        self.x = x
        self.y = y
        self.z = z


class Moon(object):
    def __init__(self, x, y, z, n):
        self.n = n
        self.pos = Coord(x, y, z)
        self.vel = Coord()

    def __repr__(self):
        return 'Moon {} (pos={},{},{}, vel={},{},{})'.format(
            self.n,
            self.pos.x,
            self.pos.y,
            self.pos.z,
            self.vel.x,
            self.vel.y,
            self.vel.z)

    def apply_gravity(self, other):
        if self.pos.x < other.pos.x:
            self.vel.x += 1
            other.vel.x -= 1
        elif self.pos.x > other.pos.x:
            self.vel.x -= 1
            other.vel.x += 1

        if self.pos.y < other.pos.y:
            self.vel.y += 1
            other.vel.y -= 1
        elif self.pos.y > other.pos.y:
            self.vel.y -= 1
            other.vel.y += 1

        if self.pos.z < other.pos.z:
            self.vel.z += 1
            other.vel.z -= 1
        elif self.pos.z > other.pos.z:
            self.vel.z -= 1
            other.vel.z += 1

    def apply_velocity(self):
        self.pos.x += self.vel.x
        self.pos.y += self.vel.y
        self.pos.z += self.vel.z

    def get_pot_energy(self):
        return abs(self.pos.x) + abs(self.pos.y) + abs(self.pos.z)

    def get_kin_energy(self):
        return abs(self.vel.x) + abs(self.vel.y) + abs(self.vel.z)

    def get_energy(self):
        return self.get_pot_energy() * self.get_kin_energy()


def load_moons():
    filename = 'input.txt'
    #filename = 'testinput.txt'

    moons = []
    for line in open(filename).readlines():
        items = line.strip()[1:-1].split(', ')
        x, y, z = [int(item.split('=')[1]) for item in items]
        n = len(moons)
        moons.append(Moon(x, y, z, n))

    return moons


moons = load_moons()

num_steps = 1000
for i in range(num_steps):
    print('Iteration {}'.format(i))
    for moon in moons:
        print(moon)

    for moon1, moon2 in itertools.combinations(moons, 2):
        moon1.apply_gravity(moon2)

    for moon in moons:
        moon.apply_velocity()

    print()

sum_energy = sum([moon.get_energy() for moon in moons])
print('Energy after {} steps: {}'.format(num_steps, sum_energy))
