#!/usr/bin/env python
# -*- coding: utf-8 -*-

start = 152085
stop = 670283

num_matching = 0

for num in range(start, stop+1):
    found_multiple = False
    last_digit = None
    decreasing = False
    for digit in str(num):
        if last_digit is not None:
            if digit == last_digit:
                found_multiple = True
            if int(digit) < int(last_digit):
                decreasing = True
        last_digit = digit

    if found_multiple and not decreasing:
        num_matching += 1

print(num_matching)
