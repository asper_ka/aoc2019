# -*- coding: utf-8 -*-

import time

test_input = """....#
#..#.
#..##
..#..
#...."""


def create_grid(text):
    lines = text.split("\n")
    sizey = len(lines)+2
    sizex = len(lines[0])+2
    grid = []
    for y in range (sizey):
        grid.append([0] * sizex)
    for y in range (1, sizey-1):
        for x in range (1, sizex-1):
            if lines[y-1][x-1] == "#":
                grid[y][x] = 1
    return grid

def print_grid(grid):
    for l in grid:
        text = ""
        for c in l:
            if c == 0:
                text += "."
            else:
                text += "#"
        print (text)

def create_next_gen(grid):
    new_grid = []
    sizey = len(grid)
    sizex = len(grid[0])
    for y in range (sizey):
        new_grid.append([0] * sizex)
    for y in range (1, sizey-1):
        for x in range (1, sizex-1):
            nbs = grid[y][x+1] + grid[y][x-1] + grid[y-1][x] + grid[y+1][x]
            if grid[y][x] ==1 and nbs != 1:
                new_grid [y][x] = 0
            elif grid[y][x] == 0 and (nbs == 1 or nbs == 2):
                new_grid[y][x] = 1
            else:
                new_grid[y][x] = grid[y][x]
    return new_grid

def calc_biodiversity(grid):
    b = 0
    idx = 0
    sizey = len(grid)
    sizex = len(grid[0])
    for y in range (1, sizey-1):
        for x in range (1, sizex-1):
            b += grid[y][x] * pow(2, idx)
            idx += 1
    return b

def run_part_one():
    text = open("input.txt").read().strip()
    #text = test_input
    grid = create_grid(text)
    bds = set()
    while True:
        b = calc_biodiversity(grid)
        if b in bds:
            break
        bds.add(b)
        grid = create_next_gen(grid)
    print_grid(grid)
    print(b)

def run_part_two():
    text = open("input.txt").read().strip()


if __name__ == "__main__":
    start_time = time.time()
    run_part_one()
    print(time.time() - start_time)
