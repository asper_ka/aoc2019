from unittest import TestCase

from sra.python.Day05.AoC_05 import parse_instruction, get_value, run_program


class Test(TestCase):
    def test_parse_instruction(self):
        opcode, modes = parse_instruction(1002)
        self.assertEqual(opcode, 2)
        self.assertEqual(len(modes), 3)
        self.assertEqual(modes[0], 0)
        self.assertEqual(modes[1], 1)
        self.assertEqual(modes[2], 0)

    def test_get_value(self):
        program = [11, 22, 33]
        self.assertEqual(get_value(2, 0, program), 33)
        self.assertEqual(get_value(14, 1, program), 14)

    def test_run_program(self):
        program = [1002, 4, 3, 4, 33]
        self.assertTrue(run_program(program,0)[0])
        self.assertEqual(program[4],99)

        program = [1101,100,-1,4,0]
        self.assertTrue(run_program(program,0)[0])
        self.assertEqual(program[4],99)

        program = [3,0,4,0,99]
        (result, output) = run_program(program,42)
        self.assertTrue(result)
        self.assertEqual(len(output),1)
        self.assertEqual(output[0],42)

    def test_run_program2_a(self):
        program = [3,9,8,9,10,9,4,9,99,-1,8]
        (result, output) = run_program(program,42)
        self.assertTrue(result)
        self.assertEqual(len(output),1)
        self.assertEqual(output[0],0)

        (result, output) = run_program(program,8)
        self.assertTrue(result)
        self.assertEqual(len(output),1)
        self.assertEqual(output[0],1)

        (result, output) = run_program(program,-4)
        self.assertTrue(result)
        self.assertEqual(len(output),1)
        self.assertEqual(output[0],0)

    def test_run_program2_b(self):
        program = [3,21,1008,21,8,20,1005,20,22,107,8,21,20,1006,20,31,
             1106,0,36,98,0,0,1002,21,125,20,4,20,1105,1,46,104,
             999,1105,1,46,1101,1000,1,20,4,20,1105,1,46,98,99]
        (result, output) = run_program(program, 4)
        self.assertTrue(result)
        self.assertEqual(len(output), 1)
        self.assertEqual(output[0], 999)
        (result, output) = run_program(program, 8)
        self.assertTrue(result)
        self.assertEqual(len(output), 1)
        self.assertEqual(output[0], 1000)
        (result, output) = run_program(program, 12)
        self.assertTrue(result)
        self.assertEqual(len(output), 1)
        self.assertEqual(output[0], 1001)
