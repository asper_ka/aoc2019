#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np


def get_visited(path):
    x, y = 0, 0
    visited = set()
    n_steps = 0
    steps_map = {}
    for segment in path:
        #print(segment)
        direction, num = segment[0], int(segment[1:])
        dx, dy = 0, 0
        if direction == 'R':
            dx = num
        if direction == 'L':
            dx = -num
        if direction == 'U':
            dy = num
        if direction == 'D':
            dy = -num

        if dx:
            #print('X')
            step = np.sign(dx)
            for cx in range(x, x + dx, step):
                x += step
                n_steps += 1
                #print(x, y)
                visited.add((x, y))
                if (x, y) not in steps_map:
                    steps_map[(x, y)] = n_steps

        if dy:
            #print('Y')
            step = np.sign(dy)
            for cy in range(y, y + dy, step):
                y += step
                n_steps += 1
                #print(x, y)
                visited.add((x, y))
                if (x, y) not in steps_map:
                    steps_map[(x, y)] = n_steps

    return visited, steps_map


#path1 = 'R8,U5,L5,D3'
#path2 = 'U7,R6,D4,L4'

#path1 = 'R75,D30,R83,U83,L12,D49,R71,U7,L72'
#path2 = 'U62,R66,U55,R34,D71,R55,D58,R83'

#path1 = 'R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51'
#path2 = 'U98,R91,D20,R16,D67,R40,U7,R15,U6,R7'

lines = open('input.txt').readlines()
path1, path2 = lines[:2]

path1 = path1.split(',')
path2 = path2.split(',')

visited1, steps_map1 = get_visited(path1)
visited2, steps_map2 = get_visited(path2)

n_steps_best = max(steps_map1.values()) + max(steps_map2.values())
crossings = visited1.intersection(visited2)

for c in crossings:
    combined_dist = steps_map1[c] + steps_map2[c]
    print(c, steps_map1[c], steps_map2[c], combined_dist)
    if combined_dist < n_steps_best:
        n_steps_best = combined_dist

print('result: {}'.format(n_steps_best))
