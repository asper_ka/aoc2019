# -*- coding: utf-8 -*-

import time
from collections import deque
from enum import Enum
from itertools import permutations

#test_input = """3,15,3,16,1002,16,10,16,1,16,15,15,4,15,99,0,0"""
#test_input = """3,23,3,24,1002,24,10,24,1002,23,-1,23,101,5,23,23,1,24,23,23,4,23,99,0,0"""
#test_input = """3,31,3,32,1002,32,10,32,1001,31,-2,31,1007,31,0,33,1002,33,7,33,1,33,31,31,1,32,31,31,4,31,99,0,0,0"""

#test_input = """3,26,1001,26,-4,26,3,27,1002,27,2,27,1,27,26,27,4,27,1001,28,-1,28,1005,28,6,99,0,0,5"""
#test_input = "3,52,1001,52,-5,52,3,53,1,52,56,54,1007,54,5,55,1005,55,26,1001,54,-5,54,1105,1,12,1,53,54,53,1008,54,0,55,1001,55,1,55,2,53,55,53,4,53,1001,56,-1,56,1005,56,6,99,0,0,0,0,10"

class Computer:
    def __init__(self, text):
        self.program = [ int(x) for x in text.split(",") ]
        self.ip = 0
        self.input = deque()
        pass
    class State(Enum):
        Output = 1
        Error = 2
        Halt = 3

    def parse_instruction(self):
        text = str(self.program[self.ip])
        opcode = int(text[-2:])
        modes = []
        for i in range(len(text) - 3, -1, -1):
            modes.append(int(text[i]))
        for i in range(len(modes), 3):
            modes.append(0)
        return opcode, modes

    def get_value(self, param, mode):
        if (mode == 0):
            return self.program[param]
        else:
            return param

    def run_program (self):
        output=[]
        while True:
            opcode, modes = self.parse_instruction()
            if opcode==99:
                return self.State.Halt, output
            if opcode==1:
                params = self.program[self.ip + 1: self.ip + 4]
                self.program[params[2]]=self.get_value(params[0], modes[0])+self.get_value(params[1], modes[1])
                self.ip+=4
            elif opcode == 2:
                params = self.program[self.ip + 1: self.ip + 4]
                self.program[params[2]] = self.get_value(params[0], modes[0]) * self.get_value(params[1], modes[1])
                self.ip += 4
            elif opcode == 3:
                param = self.program[self.ip + 1]
                i = self.input.pop()
                self.program[param] = i
                #print ("input ", i)
                self.ip += 2
            elif opcode == 4:
                param = self.program[self.ip + 1]
                o = self.get_value(param, modes[0])
                #print ("output ", o)
                output.append(o)
                self.ip += 2
                return self.State.Output, output
            elif opcode == 5:
                params = self.program[self.ip + 1:self.ip + 3]
                if self.get_value(params[0], modes[0]) != 0:
                    self.ip = self.get_value(params[1], modes[1])
                else:
                    self.ip += 3
            elif opcode == 6:
                params = self.program[self.ip + 1:self.ip + 3]
                if self.get_value(params[0], modes[0]) == 0:
                    self.ip = self.get_value(params[1], modes[1])
                else:
                    self.ip += 3
            elif opcode == 7:
                params = self.program[self.ip + 1:self.ip + 4]
                if self.get_value(params[0], modes[0]) < self.get_value(params[1], modes[1]):
                    self.program[params[2]] = 1
                else:
                    self.program[params[2]] = 0
                self.ip += 4
            elif opcode == 8:
                params = self.program[self.ip + 1:self.ip + 4]
                if self.get_value(params[0], modes[0]) == self.get_value(params[1], modes[1]):
                    self.program[params[2]] = 1
                else:
                    self.program[params[2]] = 0
                self.ip += 4

            else:
                print("ERROR")
                return self.State.Error, output



def run():
    text = open("input.txt").read()
    #text = test_input
    phases = permutations(range(5,10))
    max_output = 0
    for phase in phases:
        print (phase)
        last_output = run_phases(phase, text)
        if last_output > max_output:
            max_output = last_output
    print (max_output)


def run_phases(phase, text):
    last_output = 0
    computers = []
    for p in phase:
        c = Computer(text)
        computers.append(c)
        c.input.appendleft(p)
    current_computer = 0;
    computers[0].input.appendleft(0)
    outputs = {}
    while True:
        result, output = computers[current_computer].run_program()
        if result == Computer.State.Error:
            print("ERROR")
            return -1
        if result == Computer.State.Halt:
            print (outputs)
            return outputs[4]
        outputs[current_computer] = output[0]
        current_computer = (current_computer + 1) % 5
        computers[current_computer].input.appendleft(output[0])

if __name__ == "__main__":
    start_time = time.time()
    run()
    print(time.time() - start_time)
