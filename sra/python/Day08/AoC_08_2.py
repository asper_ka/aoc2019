# -*- coding: utf-8 -*-

import time

test_input = "0222112222120000"

def run():
    text = open("input.txt").read().strip()
    #text = test_input
    width = 25
    height = 6
    image = decode_image(text, width, height)
    print_image(image, width, height)

def decode_image(text, width, height):
    layersize = width * height
    index = 0
    image = ""
    while index < layersize:
        i = index
        while i < len(text) - 1 and text[i] == '2':
            i += layersize
        image += text[i]
        index += 1
    return image

def print_image(image, width, height):
    image = image.replace('0', ' ')
    for y in range(height):
        for x in range(width):
            print(image[y * width + x], end="")
            print(image[y * width + x], end="")
        print("")


if __name__ == "__main__":
    start_time = time.time()
    run()
    print(time.time() - start_time)
