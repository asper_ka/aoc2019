from unittest import TestCase

from sra.python.Day04.AoC_04 import has_pair, is_not_decreasing, two_adjacent_same_digits


class TestDay04(TestCase):
    def test_has_pair(self):
        self.assertTrue(has_pair("112233"))

        self.assertFalse(has_pair("123456"))
        self.assertFalse(has_pair("111234"))

    def test_is_not_decreasing(self):
        self.assertTrue(is_not_decreasing("123456"))
        self.assertTrue(is_not_decreasing("111111"))

        self.assertFalse(is_not_decreasing("123450"))

    def test_two_adjacent_same_digits(self):
        self.assertTrue(two_adjacent_same_digits("1111111"))

        self.assertFalse(two_adjacent_same_digits("123456"))
