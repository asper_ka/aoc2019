# -*- coding: utf-8 -*-

import time

def run():
    text = open("input.txt").read().strip()
    index = 0
    nr_zeros = 26*6
    result = 0
    while index < len(text):
        numbers = {'0':0, '1':0, '2':0}
        for i in range (25*6):
            numbers[text[index]]+=1
            index+=1
        if numbers['0']<nr_zeros:
            nr_zeros = numbers['0']
            result = numbers['1']*numbers['2']
    print (result)



if __name__ == "__main__":
    start_time = time.time()
    run()
    print(time.time() - start_time)
