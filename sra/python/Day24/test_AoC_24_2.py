from unittest import TestCase

from sra.python.Day24.AoC_24_2 import get_neighbors


class Test(TestCase):
    def test_get_neighbors(self):
        nbs = get_neighbors(1,1,0,5,5)
        self.assertListEqual([(0,1,0), (2,1,0), (1,0,0), (1,2,0)], nbs)

        nbs = get_neighbors(1,0,0,5,5)
        self.assertListEqual([(0,0,0), (2,0,0), (2,1,-1), (1,1,0)], nbs)

        nbs = get_neighbors(0,0,0,5,5)
        self.assertListEqual([(1,2,-1), (1,0,0), (2,1,-1), (0,1,0)], nbs)

        nbs = get_neighbors(4,4,0,5,5)
        self.assertListEqual([(3,4,0), (3,2,-1), (4,3,0), (2,3,-1)], nbs)

        nbs = get_neighbors(2,1,0,5,5)
        self.assertListEqual([(1,1,0), (3,1,0), (2,0,0), (0,0,1), (1,0,1), (2,0,1), (3,0,1), (4,0,1)], nbs)

        nbs = get_neighbors(2,3,0,5,5)
        self.assertListEqual([(1,3,0), (3,3,0), (0,4,1), (1,4,1), (2,4,1), (3,4,1), (4,4,1), (2,4,0)], nbs)

        nbs = get_neighbors(1,2,0,5,5)
        self.assertListEqual([(0,2,0), (0,0,1), (0,1,1), (0,2,1), (0,3,1), (0,4,1), (1,1,0), (1,3,0)], nbs)

        nbs = get_neighbors(3,2,0,5,5)
        self.assertListEqual([(4,0,1), (4,1,1), (4,2,1), (4,3,1), (4,4,1), (4,2,0), (3,1,0), (3,3,0)], nbs)
