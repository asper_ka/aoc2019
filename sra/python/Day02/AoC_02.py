# -*- coding: utf-8 -*-

import time

#text = """1,0,0,0,99"""
#text = """1,1,1,4,99,5,6,0,99"""
#text="1,9,10,3,2,3,11,0,99,30,40,50"

text = open("input.txt").read()

start_time = time.time()

opcodes = text.split(",")
opcodes = [ int(x) for x in opcodes ]
opcodes[1] = 12
opcodes[2] = 2
print (opcodes)

cur_pos = 0
while True:
    command = opcodes[cur_pos]
    if command==99:
        break
    elif command == 1:
        opcodes[opcodes[cur_pos+3]]=opcodes[opcodes[cur_pos+1]]+opcodes[opcodes[cur_pos+2]]
    elif command==2:
        opcodes[opcodes[cur_pos+3]]=opcodes[opcodes[cur_pos+1]]*opcodes[opcodes[cur_pos+2]]
    else:
        print ("ERROR")
    print(opcodes)
    cur_pos+=4

print (opcodes)

print (time.time() - start_time)