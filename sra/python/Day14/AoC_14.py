# -*- coding: utf-8 -*-
import math
import time

input_text = """10 ORE => 10 A
1 ORE => 1 B
7 A, 1 B => 1 C
7 A, 1 C => 1 D
7 A, 1 D => 1 E
7 A, 1 E => 1 FUEL"""

input_text2 = """171 ORE => 8 CNZTR
7 ZLQW, 3 BMBT, 9 XCVML, 26 XMNCP, 1 WPTQ, 2 MZWV, 1 RJRHP => 4 PLWSL
114 ORE => 4 BHXH
14 VRPVC => 6 BMBT
6 BHXH, 18 KTJDG, 12 WPTQ, 7 PLWSL, 31 FHTLT, 37 ZDVW => 1 FUEL
6 WPTQ, 2 BMBT, 8 ZLQW, 18 KTJDG, 1 XMNCP, 6 MZWV, 1 RJRHP => 6 FHTLT
15 XDBXC, 2 LTCX, 1 VRPVC => 6 ZLQW
13 WPTQ, 10 LTCX, 3 RJRHP, 14 XMNCP, 2 MZWV, 1 ZLQW => 1 ZDVW
5 BMBT => 4 WPTQ
189 ORE => 9 KTJDG
1 MZWV, 17 XDBXC, 3 XCVML => 2 XMNCP
12 VRPVC, 27 CNZTR => 2 XDBXC
15 KTJDG, 12 BHXH => 5 XCVML
3 BHXH, 2 VRPVC => 7 MZWV
121 ORE => 7 VRPVC
7 XCVML => 6 RJRHP
5 BHXH, 4 VRPVC => 5 LTCX"""

input_text3 = """9 ORE => 2 A
8 ORE => 3 B
7 ORE => 5 C
3 A, 4 B => 1 AB
5 B, 7 C => 1 BC
4 C, 1 A => 1 CA
2 AB, 3 BC, 4 CA => 1 FUEL"""

class Reaction:
    def __init__(self, output, nr, input):
        self.output = output
        self.nr = nr
        self.input = input

    def __str__(self):
        str = "{0} {1}:".format(self.nr, self.output)
        for i in self.input.keys():
            str = str + " {0}*{1}".format(self.input[i], i)
        return str

class Reactor:
    def __init__(self):
        self.reactions = {}
        self.stock = {}

    class OutOfStock(Exception):
        pass

    def create_reactions(self, text):
        for l in text.split("\n"):
            self.parse_line(l)

    def create_stock(self):
        for r in self.reactions.keys():
            self.stock[r] = 0
            for i in self.reactions[r].input.keys():
                self.stock[i] = 0

    def parse_line(self, line):
        input, output = tuple(line.split("=>"))
        inputs = {}
        for item in input.split(","):
            i = self.extract_chemical(item)
            inputs[i[1]] = i[0]
        nr, outChemical = self.extract_chemical(output)
        self.reactions[outChemical] = Reaction(outChemical, nr, inputs)

    def extract_chemical(self, text):
        items = text.split()
        return int(items[0]), items[1]

    def is_output_only(self, chemical):
        for r in self.reactions.keys():
            if chemical in self.reactions[r].input:
                return False
        return True

    @staticmethod
    def combine_reaction(reaction, in_reaction):
        inputs = reaction.input
        nr_needed = reaction.input[in_reaction.output]
        factor = math.ceil(nr_needed / in_reaction.nr)
        for chem in in_reaction.input.keys():
            if chem in inputs:
                inputs[chem] += factor * in_reaction.input[chem]
            else:
                inputs[chem] = factor * in_reaction.input[chem]
        del inputs[in_reaction.output]
        return Reaction(reaction.output, reaction.nr, inputs)

    def produce_chemicals(self, chemical, nr_produce):
        if not chemical in self.reactions:
            raise Reactor.OutOfStock()
        reaction = self.reactions[chemical]
        nr_reactions = math.ceil(nr_produce / reaction.nr)
        for needed_chem in reaction.input.keys():
            nr_in_stock = self.stock[needed_chem]
            nr_needed = reaction.input[needed_chem]*nr_reactions
            if nr_in_stock<nr_needed:
                nr_needed -= nr_in_stock
                self.stock[needed_chem] = 0
                self.produce_chemicals(needed_chem, nr_needed)
            self.stock[needed_chem] -= nr_needed
        self.stock[chemical] += nr_reactions*reaction.nr

def run_part2():
    text = open("input.txt").read().strip()
    #text = input_text2
    reactor = Reactor()
    reactor.create_reactions(text)
    reactor.create_stock()
    reactor.stock["ORE"] = 1000000000000
    steps = 100000
    while steps > 0.5:
        try:
            while True:
                stock_before_exception = reactor.stock.copy()
                reactor.produce_chemicals("FUEL", steps)
        except Reactor.OutOfStock:
            steps = steps / 10
            # when thrown ORE will be 0 but it's too complicated to unwind this, so start from last good position
            reactor.stock = stock_before_exception.copy()
    print (reactor.stock["FUEL"])


def run_part1():
    text = open("input.txt").read().strip()
    #text = input_text
    reactor = Reactor()
    reactor.create_reactions(text)
    fuel_reaction = reactor.reactions["FUEL"]
    del reactor.reactions["FUEL"]
    #print(fuel_reaction)
    while len(fuel_reaction.input) > 1:
        for chem in fuel_reaction.input.keys():
            if reactor.is_output_only(chem):
                fuel_reaction = Reactor.combine_reaction(fuel_reaction, reactor.reactions[chem])
                del reactor.reactions[chem]
                break;
    print (fuel_reaction)

def run():
    run_part2()

if __name__ == "__main__":
    start_time = time.time()
    run()
    print(time.time() - start_time)
