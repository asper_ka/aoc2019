# -*- coding: utf-8 -*-

import time

test_grid = """.#....#####...#..
##...##.#####..##
##...#...#.#####.
..#.....X...###..
..#.#.....#....##"""

test_grid2 = """.#..##.###...#######
##.############..##.
.#.######.########.#
.###.#######.####.#.
#####.##.#.##.###.##
..#####..#.#########
####################
#.####....###.#.#.##
##.#################
#####.##.###..####..
..######..##.#######
####.##.####...##..#
.#####..#.######.###
##...#.##########...
#.##########.#######
.####.#.###.###.#.##
....##.##.###..#####
.#.#.###########.###
#.#.#.#####.####.###
###.##.####.##.#..##"""

class Coord:
    def __init__(self, x, y):
        self.x = int(x)
        self.y = int(y)
    def equals(self,c):
        return self.x == c.x and self.y == c.y
    def clone(self):
        return Coord(self.x, self.y)
    def __repr__(self):
        return "("+str(self.x)+"/"+str(self.y)+")"
    def __eq__(self, other):
        return self.x == other.x and self.y == other.y
    def __add__(self, other):
        return Coord(self.x + other.x , self.y + other.y)
    def __sub__(self, other):
        return Coord(self.x - other.x , self.y - other.y)
    def in_grid(self, grid):
        if len(grid)==0:
            return False
        return self.x>=0 and self.x<len(grid[0]) and self.y>=0 and self.y < len(grid)


def gcd(x, y):
    while (y):
        x, y = y, x % y
    return x

def get_dir_vector(dx,dy):
    d = abs(gcd(dx,dy))
    return (int(dx/d), int(dy/d))

def create_grid(text):
    grid = []
    for l in text.split():
        grid.append(list(l))
    return grid

def in_sight(c1, c2, grid):
    (dx,dy) = get_dir_vector((c2.x-c1.x), (c2.y-c1.y))
    pos = c2.clone()
    while not pos.equals(c1):
        pos.x -= dx
        pos.y -= dy
        if not pos.equals(c1) and grid[pos.y][pos.x]=='#':
            return False
    return True

def count_in_sight(c1,grid):
    count = 0
    for y in range(len(grid)):
        for x in range(len(grid[0])):
            c2 = Coord(x,y)
            if not c1.equals(c2) and grid[y][x]=="#" and in_sight(c1,c2,grid):
                count += 1
    return count

def get_next_farey_sequence(seq, n):
    # https://de.wikipedia.org/wiki/Farey-Folge Methode 2
    idx = []
    for c in range(len(seq)-2, -1, -1):
        if (seq[c].y + seq[c+1].y) == n:
            idx.append(c+1)
    for c in idx:
        seq.insert(c, Coord(seq[c-1].x + seq[c].x, seq[c-1].y + seq[c].y))
    return seq

def farey_sequence(n):
    seq = [Coord(0,1), Coord(1,1)]
    for i in range (2, n+1):
        seq = get_next_farey_sequence(seq, i)
    return seq

def count_asteroids(grid):
    sum = 0
    for l in grid:
        sum += l.count('#')
    return sum

def run():
    text = open("input.txt").read().strip()
    #text = test_grid2
    grid = create_grid(text)
    dirs = create_directions(max(len(grid), len(grid[0])))

    laser = Coord(20,19)
    cnt = 1
    while count_asteroids(grid)>0:
        for d in dirs:
            p = laser + d
            while p.in_grid(grid):
                if grid[p.y][p.x] == "#":
                    grid[p.y][p.x] = "."
                    if cnt == 200:
                        print (cnt, p)
                    cnt += 1
                    break
                p += d


def create_directions(n):
    # the farey sequence gives us the first 1/8
    seq = farey_sequence(n)[1:-1]
    dirs = [Coord(0, -1)]
    for s in seq:
        dirs.append(Coord(s.x, -s.y))
    dirs.append(Coord(1, -1))
    # next quarter is the sequence backwarts
    for s in reversed(seq):
        dirs.append(Coord(s.y, -s.x))
    dirlen = len(dirs)
    # next 1/4 is the first on rotated by 90 degree
    for i in range(dirlen):
        d = dirs[i]
        dirs.append(Coord(-d.y, d.x))
    dirlen = len(dirs)
    # second 1/2 is the first one mirrord at origin
    for i in range(dirlen):
        d = dirs[i]
        dirs.append(Coord(-d.x, -d.y))
    return dirs


if __name__ == "__main__":
    start_time = time.time()
    run()
    print(time.time() - start_time)
