# -*- coding: utf-8 -*-

import time

#text = """1,0,0,0,99"""
#text = """1,1,1,4,99,5,6,0,99"""
#text="1,9,10,3,2,3,11,0,99,30,40,50"

text = open("input.txt").read()

start_time = time.time()

def run_program (noun, verb):
    program = text.split(",")
    program = [ int(x) for x in program ]
    program[1] = noun
    program[2] = verb
    #print (program)

    ip = 0 # instruction pointer
    while True:
        opcode = program[ip]
        if opcode==99:
            return program[0]
        params = program[ip+1: ip+4]
        if opcode==1:
            program[params[2]]=program[params[0]]+program[params[1]]
        elif opcode==2:
            program[params[2]]=program[params[0]]*program[params[1]]
        else:
            print("ERROR")
            return -1
        #print(program)
        ip+=4

print("Part one {0}".format(run_program(12,2)))

for noun in range(0,100):
    for verb in range (0,100):
        if run_program(noun, verb)==19690720:
            print ("Part two {0}".format(noun*100+ verb))
            break

print (time.time() - start_time)