from unittest import TestCase

from sra.python.Day06.AoC_06 import get_objects


class Test(TestCase):
    def test_get_objects(self):
        o1,o2 = get_objects("COM)A")
        self.assertEqual(o1, "COM")
        self.assertEqual(o2, "A")
