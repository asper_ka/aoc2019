# -*- coding: utf-8 -*-

import time
from sra.python.Computer import Computer


def send_to_computer(computer, cmds, run=False):
    text = "\n".join(cmds)
    if run:
        text += "\nRUN\n"
    else:
        text += "\nWALK\n"
    print(text)
    for c in text:
        computer.input.appendleft(ord(c))


def run():
    text = open("input.txt").read().strip()
    computer = Computer(text)
    result, output = computer.run_program()
    print(''.join([str(chr(x)) for x in output]))
    print(result)
    # part one:
    # send_to_computer(computer, "NOT A J\nNOT B T\nOR T J\nNOT C T\nOR T J\nAND D J\nWALK\n")
    # part two
    send_to_computer(computer,
                     ["NOT A J", "NOT B T", "OR T J", "NOT C T", "OR T J", "AND D J", "NOT J T", "OR H T", "OR E T",
                      "AND T J"], True)
    result, output = computer.run_program()
    display = ""
    for c in output:
        if c < 255:
            display += str(chr(c))
        else:
            display += str(c)
    print(display)
    print(result)


if __name__ == "__main__":
    start_time = time.time()
    run()
    print(time.time() - start_time)
