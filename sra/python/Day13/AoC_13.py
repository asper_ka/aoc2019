# -*- coding: utf-8 -*-
import operator
import time
from collections import deque
from enum import Enum

from sra.python.Computer import Computer



def run():
    text = open("input.txt").read()
    computer = Computer(text)
    result , output = computer.run_program()
    print (result, output)
    tiles = {}
    for i in range(0, len(output), 3):
        tiles [(output[i], output[i+1])] = output[i+2]
    print (tiles)
    types = list(tiles.values())
    print (types)
    print (types.count(2))



if __name__ == "__main__":
    start_time = time.time()
    run()
    print(time.time() - start_time)
